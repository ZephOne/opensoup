#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;

pub mod api;
pub mod db;

pub fn opensoup_rocket() -> rocket::Rocket {
    rocket::ignite()
        .attach(db::DbConnection::fairing())
        .mount(
            "/cook",
            routes![
                api::cook::create,
                api::cook::read,
                api::cook::update,
                api::cook::delete,
                api::cook::all,
                api::cook::all_recipes_by_cook,
                api::cook::all_recipe_usage_by_cook,
                api::cook::recipe_usage_by_cook,
                api::cook::all_role_by_cook,
            ],
        )
        .mount(
            "/ingredient",
            routes![
                api::ingredient::create,
                api::ingredient::read,
                api::ingredient::update,
                api::ingredient::delete,
                api::ingredient::all
            ],
        )
        .mount(
            "/recipe",
            routes![
                api::recipe::create,
                api::recipe::read,
                api::recipe::update,
                api::recipe::delete,
                api::recipe::all,
                api::recipe::all_ingredient_by_recipe
            ],
        )
        .mount(
            "/role",
            routes![
                api::role::create,
                api::role::read,
                api::role::update,
                api::role::delete,
                api::role::all,
                api::role::all_cook_by_role
            ],
        )
        .mount(
            "/unit",
            routes![
                api::unit::create,
                api::unit::read,
                api::unit::update,
                api::unit::delete,
                api::unit::all,
            ],
        )
        .mount(
            "/time_to_cook_label",
            routes![
                api::time_to_cook_label::create,
                api::time_to_cook_label::read,
                api::time_to_cook_label::update,
                api::time_to_cook_label::delete,
                api::time_to_cook_label::all,
            ],
        )
    // .mount(
    // "/complete_recipe",
    // routes![
    // api::complete_recipe::create,
    // ],
    // )
}
