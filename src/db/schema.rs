table! {
    cook_roles (id) {
        id -> Int4,
        cook_id -> Int4,
        role_id -> Int4,
    }
}

table! {
    cooks (id) {
        id -> Int4,
        email -> Text,
        name -> Text,
        password -> Text,
    }
}

table! {
    ingredient_recipe_units (id) {
        id -> Int4,
        portions -> Int4,
        quantity -> Nullable<Text>,
        ingredient_id -> Int4,
        recipe_id -> Int4,
        unit_id -> Nullable<Int4>,
    }
}

table! {
    ingredients (id) {
        id -> Int4,
        name -> Text,
    }
}

table! {
    recipe_cooks (id) {
        id -> Int4,
        use_date -> Date,
        recipe_id -> Int4,
        cook_id -> Int4,
    }
}

table! {
    recipes (id) {
        id -> Int4,
        name -> Text,
        time_to_cook_label_id -> Nullable<Int4>,
        content -> Text,
        creation_date -> Timestamp,
        update_date -> Nullable<Timestamp>,
        cook_id -> Int4,
    }
}

table! {
    roles (id) {
        id -> Int4,
        label -> Text,
    }
}

table! {
    time_to_cook_labels (id) {
        id -> Int4,
        name -> Text,
    }
}

table! {
    units (id) {
        id -> Int4,
        name -> Text,
        symbol -> Nullable<Text>,
    }
}

joinable!(cook_roles -> cooks (cook_id));
joinable!(cook_roles -> roles (role_id));
joinable!(ingredient_recipe_units -> ingredients (ingredient_id));
joinable!(ingredient_recipe_units -> recipes (recipe_id));
joinable!(ingredient_recipe_units -> units (unit_id));
joinable!(recipe_cooks -> cooks (cook_id));
joinable!(recipe_cooks -> recipes (recipe_id));
joinable!(recipes -> cooks (cook_id));

allow_tables_to_appear_in_same_query!(
    cook_roles,
    cooks,
    ingredient_recipe_units,
    ingredients,
    recipe_cooks,
    recipes,
    roles,
    time_to_cook_labels,
    units,
);
