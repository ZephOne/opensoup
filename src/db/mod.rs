use std::fs::File;
use std::io;
use std::io::prelude::*;

use diesel::{Connection, PgConnection};
use rocket_contrib::databases::diesel;
use toml::Value;

pub mod models;
pub mod schema;

#[database("db")]
#[derive(Connection)]
pub struct DbConnection(PgConnection);

pub fn get_config_file_content() -> Result<String, io::Error> {
    let mut config_file = File::open("Rocket.toml")?;
    let mut config_file_content = String::new();
    config_file.read_to_string(&mut config_file_content)?;
    Ok(config_file_content)
}

pub fn establish_db_connection() -> PgConnection {
    let config_file_content = get_config_file_content().expect("Can't read Rocket.toml");
    let config = config_file_content
        .parse::<Value>()
        .expect("Your Rocket.toml file is not correct");
    let database_url = &config["global"]["databases"]["db"]["url"].as_str().unwrap();
    dbg!(database_url);
    PgConnection::establish(database_url).expect(&format!("Error connecting to {}", database_url))
}
