use chrono::{NaiveDate, NaiveDateTime};
use diesel::dsl::any;
use diesel::{self, prelude::*};
use serde::{Deserialize, Serialize};

use super::schema::cooks::dsl::cooks as all_cook;
use super::schema::ingredient_recipe_units::dsl::ingredient_recipe_units as all_ingredient_recipe_unit;
use super::schema::ingredients::dsl::ingredients as all_ingredient;
use super::schema::recipes::dsl::recipes as all_recipe;
use super::schema::roles::dsl::roles as all_role;
use super::schema::time_to_cook_labels::dsl::time_to_cook_labels as all_time_to_cook_label;
use super::schema::units::dsl::units as all_unit;
use super::schema::{
    cook_roles, cooks, ingredient_recipe_units, ingredients, recipe_cooks, recipes, roles,
    time_to_cook_labels, units,
};

#[derive(AsChangeset, Deserialize, Identifiable, Queryable, Serialize)]
#[table_name = "cooks"]
pub struct Cook {
    pub id: i32,
    pub email: String,
    pub name: String,
    pub password: String,
}

impl Cook {
    pub fn all(db_connection: &PgConnection) -> Vec<Cook> {
        all_cook
            .order(cooks::email.desc())
            .load::<Cook>(db_connection)
            .expect("Error getting all cooks")
    }

    pub fn get(db_connection: &PgConnection, id: i32) -> QueryResult<Cook> {
        cooks::table.find(id).get_result::<Cook>(db_connection)
    }

    pub fn update(db_connection: &PgConnection, id: i32, cook: Cook) -> QueryResult<Cook> {
        diesel::update(cooks::table.find(id))
            .set(&cook)
            .get_result(db_connection)
    }

    pub fn delete(db_connection: &PgConnection, id: &i32) -> QueryResult<usize> {
        diesel::delete(all_cook.filter(cooks::columns::id.eq(id))).execute(db_connection)
    }
}

#[derive(Deserialize, Insertable)]
#[table_name = "cooks"]
pub struct NewCook {
    pub email: String,
    pub name: String,
    pub password: String,
}

impl NewCook {
    pub fn create(db_connection: &PgConnection, new_cook: NewCook) -> QueryResult<Cook> {
        diesel::insert_into(cooks::table)
            .values(&new_cook)
            .get_result(db_connection)
    }
}

#[derive(AsChangeset, Deserialize, Identifiable, Queryable, Serialize)]
#[table_name = "roles"]
pub struct Role {
    pub id: i32,
    pub label: String,
}

impl Role {
    pub fn all(db_connection: &PgConnection) -> Vec<Role> {
        all_role
            .order(roles::label.desc())
            .load::<Role>(db_connection)
            .expect("Error getting all roles")
    }

    pub fn get(db_connection: &PgConnection, id: i32) -> QueryResult<Role> {
        roles::table.find(id).get_result::<Role>(db_connection)
    }

    pub fn get_by_label(db_connection: &PgConnection, label: String) -> QueryResult<Role> {
        all_role
            .filter(roles::label.eq(label))
            .get_result::<Role>(db_connection)
    }

    pub fn update(db_connection: &PgConnection, id: i32, role: Role) -> QueryResult<Role> {
        diesel::update(roles::table.find(id))
            .set(&role)
            .get_result(db_connection)
    }

    pub fn delete(db_connection: &PgConnection, id: &i32) -> QueryResult<usize> {
        diesel::delete(all_role.filter(roles::columns::id.eq(id))).execute(db_connection)
    }
}

#[derive(Deserialize, Insertable)]
#[table_name = "roles"]
pub struct NewRole {
    pub label: String,
}

impl NewRole {
    pub fn create(db_connection: &PgConnection, new_role: NewRole) -> QueryResult<Role> {
        diesel::insert_into(roles::table)
            .values(&new_role)
            .get_result(db_connection)
    }
}

#[derive(Associations, Identifiable, Queryable, Serialize)]
#[belongs_to(Cook)]
#[belongs_to(Role)]
#[table_name = "cook_roles"]
pub struct CookRole {
    pub id: i32,
    pub cook_id: i32,
    pub role_id: i32,
}

impl CookRole {
    pub fn all_cook_by_role(db_connection: &PgConnection, role_id: i32) -> QueryResult<Vec<Cook>> {
        match Role::get(db_connection, role_id) {
            Ok(role) => {
                let cook_ids = CookRole::belonging_to(&role).select(cook_roles::cook_id);
                cooks::table
                    .filter(cooks::id.eq(any(cook_ids)))
                    .load::<Cook>(db_connection)
            }
            Err(error) => Err(error),
        }
    }

    pub fn all_role_by_cook(db_connection: &PgConnection, cook_id: i32) -> QueryResult<Vec<Role>> {
        match Cook::get(db_connection, cook_id) {
            Ok(cook) => {
                let role_ids = CookRole::belonging_to(&cook).select(cook_roles::role_id);
                roles::table
                    .filter(roles::id.eq(any(role_ids)))
                    .load::<Role>(db_connection)
            }
            Err(error) => Err(error),
        }
    }
}

#[derive(Associations, Deserialize, Insertable)]
#[belongs_to(Cook)]
#[belongs_to(Role)]
#[table_name = "cook_roles"]
pub struct NewCookRole {
    pub cook_id: i32,
    pub role_id: i32,
}

impl NewCookRole {
    pub fn create(
        db_connection: &PgConnection,
        new_cook_role: NewCookRole,
    ) -> QueryResult<CookRole> {
        diesel::insert_into(cook_roles::table)
            .values(&new_cook_role)
            .get_result(db_connection)
    }
}

#[derive(AsChangeset, Deserialize, Identifiable, Queryable, Serialize)]
#[table_name = "time_to_cook_labels"]
pub struct TimeToCookLabel {
    pub id: i32,
    pub name: String,
}

impl TimeToCookLabel {
    pub fn all(db_connection: &PgConnection) -> Vec<TimeToCookLabel> {
        all_time_to_cook_label
            .order(time_to_cook_labels::name.desc())
            .load::<TimeToCookLabel>(db_connection)
            .expect("Error getting all time_to_cook_labels")
    }

    pub fn get(db_connection: &PgConnection, id: i32) -> QueryResult<TimeToCookLabel> {
        time_to_cook_labels::table
            .find(id)
            .get_result::<TimeToCookLabel>(db_connection)
    }

    pub fn get_by_name(db_connection: &PgConnection, name: String) -> QueryResult<TimeToCookLabel> {
        all_time_to_cook_label
            .filter(time_to_cook_labels::name.eq(name))
            .get_result::<TimeToCookLabel>(db_connection)
    }

    pub fn update(
        db_connection: &PgConnection,
        id: i32,
        time_to_cook_label: TimeToCookLabel,
    ) -> QueryResult<TimeToCookLabel> {
        diesel::update(time_to_cook_labels::table.find(id))
            .set(&time_to_cook_label)
            .get_result(db_connection)
    }

    pub fn delete(db_connection: &PgConnection, id: &i32) -> QueryResult<usize> {
        diesel::delete(all_time_to_cook_label.filter(time_to_cook_labels::columns::id.eq(id)))
            .execute(db_connection)
    }
}

#[derive(Deserialize, Insertable, Serialize)]
#[table_name = "time_to_cook_labels"]
pub struct NewTimeToCookLabel {
    pub name: String,
}

impl NewTimeToCookLabel {
    pub fn create(
        db_connection: &PgConnection,
        new_time_to_cook_label: NewTimeToCookLabel,
    ) -> QueryResult<TimeToCookLabel> {
        diesel::insert_into(time_to_cook_labels::table)
            .values(&new_time_to_cook_label)
            .get_result(db_connection)
    }
}

#[derive(AsChangeset, Associations, Identifiable, Queryable, Serialize, Deserialize)]
#[belongs_to(TimeToCookLabel)]
#[belongs_to(Cook)]
#[table_name = "recipes"]
pub struct Recipe {
    pub id: i32,
    pub name: String,
    pub time_to_cook_label_id: Option<i32>,
    pub content: String,
    pub creation_date: NaiveDateTime,
    pub update_date: Option<NaiveDateTime>,
    pub cook_id: i32,
}

impl Recipe {
    pub fn all(db_connection: &PgConnection) -> Vec<Recipe> {
        all_recipe
            .order(recipes::creation_date.desc())
            .load::<Recipe>(db_connection)
            .expect("Error getting all recipes")
    }

    pub fn all_by_cook(db_connection: &PgConnection, cook_id: i32) -> Vec<Recipe> {
        all_recipe
            .order(recipes::creation_date.desc())
            .filter(recipes::cook_id.eq(cook_id))
            .load::<Recipe>(db_connection)
            .expect("Error getting cook's recipes")
    }

    pub fn get(db_connection: &PgConnection, id: i32) -> QueryResult<Recipe> {
        recipes::table.find(id).get_result::<Recipe>(db_connection)
    }

    pub fn get_by_name(db_connection: &PgConnection, name: String) -> QueryResult<Recipe> {
        recipes::table
            .filter(recipes::name.eq(name))
            .get_result::<Recipe>(db_connection)
    }

    pub fn update(db_connection: &PgConnection, recipe: Recipe) -> QueryResult<Recipe> {
        diesel::update(recipes::table.find(&recipe.id))
            .set(&recipe)
            .get_result(db_connection)
    }

    pub fn delete(db_connection: &PgConnection, id: &i32) -> QueryResult<usize> {
        diesel::delete(all_recipe.filter(recipes::columns::id.eq(id))).execute(db_connection)
    }
}

#[derive(Associations, Clone, Insertable, Deserialize)]
#[belongs_to(TimeToCookLabel)]
#[belongs_to(Cook)]
#[table_name = "recipes"]
pub struct NewRecipe {
    pub name: String,
    pub time_to_cook_label_id: Option<i32>,
    pub content: String,
    pub cook_id: i32,
}

impl NewRecipe {
    pub fn create(db_connection: &PgConnection, new_recipe: NewRecipe) -> QueryResult<Recipe> {
        diesel::insert_into(recipes::table)
            .values(&new_recipe)
            .get_result(db_connection)
    }
}

#[derive(Associations, Identifiable, Queryable, Serialize)]
#[belongs_to(Recipe)]
#[belongs_to(Cook)]
#[table_name = "recipe_cooks"]
pub struct RecipeCook {
    pub id: i32,
    pub use_date: NaiveDate,
    pub recipe_id: i32,
    pub cook_id: i32,
}

impl RecipeCook {
    pub fn all_recipe_usage_by_cook(
        db_connection: &PgConnection,
        cook_id: i32,
    ) -> QueryResult<Vec<RecipeCook>> {
        match Cook::get(db_connection, cook_id) {
            Ok(cook) => RecipeCook::belonging_to(&cook).load::<RecipeCook>(db_connection),
            Err(error) => Err(error),
        }
    }

    pub fn recipe_usage_by_cook(
        db_connection: &PgConnection,
        cook_id: i32,
        recipe_id: i32,
    ) -> QueryResult<Vec<RecipeCook>> {
        match Cook::get(db_connection, cook_id) {
            Ok(cook) => RecipeCook::belonging_to(&cook)
                .filter(recipe_cooks::recipe_id.eq(recipe_id))
                .load::<RecipeCook>(db_connection),
            Err(error) => Err(error),
        }
    }
}

#[derive(Associations, Insertable, Deserialize)]
#[belongs_to(Recipe)]
#[belongs_to(Cook)]
#[table_name = "recipe_cooks"]
pub struct NewRecipeCook {
    pub use_date: NaiveDate,
    pub recipe_id: i32,
    pub cook_id: i32,
}

impl NewRecipeCook {
    pub fn create(
        db_connection: &PgConnection,
        new_recipe_cook: NewRecipeCook,
    ) -> QueryResult<RecipeCook> {
        diesel::insert_into(recipe_cooks::table)
            .values(&new_recipe_cook)
            .get_result(db_connection)
    }
}

#[derive(AsChangeset, Deserialize, Identifiable, Queryable, Serialize)]
#[table_name = "ingredients"]
pub struct Ingredient {
    pub id: i32,
    pub name: String,
}

impl Ingredient {
    pub fn all(db_connection: &PgConnection) -> Vec<Ingredient> {
        all_ingredient
            .order(ingredients::name.asc())
            .load::<Ingredient>(db_connection)
            .expect("Error getting all ingredients")
    }

    pub fn all_by_recipe(db_connection: &PgConnection, recipe_id: i32) -> Vec<Ingredient> {
        let ingredients_ids = all_ingredient_recipe_unit
            .select(ingredient_recipe_units::ingredient_id)
            .filter(ingredient_recipe_units::recipe_id.eq(recipe_id))
            .load::<i32>(db_connection)
            .expect("Unable to load any ingredients using a recipe");
        all_ingredient
            .filter(ingredients::id.eq(any(ingredients_ids)))
            .load::<Ingredient>(db_connection)
            .expect("Error getting ingredients by recipe")
    }

    pub fn get(db_connection: &PgConnection, id: i32) -> QueryResult<Ingredient> {
        ingredients::table
            .find(id)
            .get_result::<Ingredient>(db_connection)
    }

    pub fn get_by_name(db_connection: &PgConnection, name: String) -> QueryResult<Ingredient> {
        all_ingredient
            .filter(ingredients::name.eq(name))
            .get_result::<Ingredient>(db_connection)
    }

    pub fn update(
        db_connection: &PgConnection,
        id: i32,
        ingredient: Ingredient,
    ) -> QueryResult<Ingredient> {
        diesel::update(ingredients::table.find(id))
            .set(&ingredient)
            .get_result(db_connection)
    }

    pub fn delete(db_connection: &PgConnection, id: &i32) -> QueryResult<usize> {
        diesel::delete(all_ingredient.filter(ingredients::columns::id.eq(id)))
            .execute(db_connection)
    }
}

#[derive(Insertable, Deserialize)]
#[table_name = "ingredients"]
pub struct NewIngredient {
    pub name: String,
}

impl NewIngredient {
    pub fn create(
        db_connection: &PgConnection,
        new_ingredient: NewIngredient,
    ) -> QueryResult<Ingredient> {
        diesel::insert_into(ingredients::table)
            .values(&new_ingredient)
            .get_result(db_connection)
    }
}

#[derive(AsChangeset, Deserialize, Identifiable, Queryable, Serialize)]
#[table_name = "units"]
pub struct Unit {
    pub id: i32,
    pub name: String,
    pub symbol: Option<String>,
}

impl Unit {
    pub fn all(db_connection: &PgConnection) -> Vec<Unit> {
        all_unit
            .order(units::name.asc())
            .load::<Unit>(db_connection)
            .expect("Error getting all units")
    }

    pub fn get(db_connection: &PgConnection, id: i32) -> QueryResult<Unit> {
        all_unit.find(id).get_result::<Unit>(db_connection)
    }

    pub fn get_by_name(db_connection: &PgConnection, name: String) -> QueryResult<Unit> {
        all_unit
            .filter(units::name.eq(name))
            .get_result::<Unit>(db_connection)
    }

    pub fn update(db_connection: &PgConnection, id: i32, unit: Unit) -> QueryResult<Unit> {
        diesel::update(units::table.find(id))
            .set(&unit)
            .get_result(db_connection)
    }

    pub fn delete(db_connection: &PgConnection, id: &i32) -> QueryResult<usize> {
        diesel::delete(all_unit.filter(units::columns::id.eq(id))).execute(db_connection)
    }
}

#[derive(Deserialize, Insertable, Serialize)]
#[table_name = "units"]
pub struct NewUnit {
    pub name: String,
    pub symbol: Option<String>,
}

impl NewUnit {
    pub fn create(db_connection: &PgConnection, new_unit: NewUnit) -> QueryResult<Unit> {
        diesel::insert_into(units::table)
            .values(&new_unit)
            .get_result(db_connection)
    }
}

#[derive(Associations, Identifiable, Queryable)]
#[belongs_to(Ingredient)]
#[belongs_to(Recipe)]
#[belongs_to(Unit)]
#[table_name = "ingredient_recipe_units"]
pub struct IngredientRecipeUnit {
    pub id: i32,
    pub portions: i32,
    pub quantity: Option<String>,
    pub ingredient_id: i32,
    pub recipe_id: i32,
    pub unit_id: Option<i32>,
}

#[derive(Associations, Insertable, Queryable)]
#[belongs_to(Ingredient)]
#[belongs_to(Recipe)]
#[belongs_to(Unit)]
#[table_name = "ingredient_recipe_units"]
pub struct NewIngredientRecipeUnit {
    pub portions: i32,
    pub quantity: Option<String>,
    pub ingredient_id: i32,
    pub recipe_id: i32,
    pub unit_id: Option<i32>,
}

impl NewIngredientRecipeUnit {
    pub fn create(
        db_connection: &PgConnection,
        new_ingredient_recipe_unit: NewIngredientRecipeUnit,
    ) -> QueryResult<usize> {
        diesel::insert_into(ingredient_recipe_units::table)
            .values(&new_ingredient_recipe_unit)
            .execute(db_connection)
    }

    pub fn create_several(
        db_connection: &PgConnection,
        new_ingredient_recipe_units: &Vec<NewIngredientRecipeUnit>,
    ) -> QueryResult<usize> {
        diesel::insert_into(ingredient_recipe_units::table)
            .values(new_ingredient_recipe_units)
            .execute(db_connection)
    }
}
