use diesel::connection::Connection;
use diesel::result::Error;
use rocket::response::status;
use serde::Deserialize;

use crate::api::Json;
use crate::db::models::{NewIngredientRecipeUnit, NewRecipe};
use crate::db::DbConnection;

#[derive(Deserialize)]
pub struct IngredientUsage {
    ingredient_id: i32,
    unit_id: Option<i32>,
    quantity: Option<String>,
}

#[derive(Deserialize)]
pub struct CompleteRecipe {
    name: String,
    content: String,
    time_to_cook_label_id: Option<i32>,
    cook_id: i32,
    ingredients_usage: Vec<IngredientUsage>,
    portions: i32,
}

fn create_recipe_and_ingredient_unit_associations(
    new_recipe: NewRecipe,
    complete_recipe_json: &Json<CompleteRecipe>,
    db_connection: &DbConnection,
) -> Result<i32, Error> {
    match NewRecipe::create(&db_connection, new_recipe) {
        Ok(recipe) => {
            let mut ingredient_recipe_units_vec: Vec<NewIngredientRecipeUnit> = vec![];
            for ingredient_usage in &complete_recipe_json.ingredients_usage {
                let quantity: Option<String>;
                if let Some(ingredient_quantity) = ingredient_usage.quantity.as_ref() {
                    let str_quantity = ingredient_quantity.as_str();
                    quantity = Some(String::from(str_quantity));
                } else {
                    quantity = None;
                }
                let new_ingredient_recipe_unit = NewIngredientRecipeUnit {
                    portions: complete_recipe_json.portions,
                    quantity: quantity,
                    ingredient_id: ingredient_usage.ingredient_id,
                    recipe_id: recipe.id,
                    unit_id: ingredient_usage.unit_id,
                };
                &ingredient_recipe_units_vec.push(new_ingredient_recipe_unit);
            }
            if let Err(error) = NewIngredientRecipeUnit::create_several(
                &db_connection,
                &ingredient_recipe_units_vec,
            ) {
                return Err(error);
            }
            Ok(recipe.id)
        }
        Err(error) => Err(error),
    }
}

#[post("/", format = "json", data = "<complete_recipe>")]
pub fn create(
    db_connection: DbConnection,
    complete_recipe: Json<CompleteRecipe>,
) -> Result<status::Created<String>, String> {
    let new_recipe = NewRecipe {
        name: String::from(&complete_recipe.name),
        content: String::from(&complete_recipe.content),
        time_to_cook_label_id: complete_recipe.time_to_cook_label_id,
        cook_id: complete_recipe.cook_id,
    };
    let transaction = db_connection.transaction::<i32, Error, _>(|| {
        create_recipe_and_ingredient_unit_associations(new_recipe, &complete_recipe, &db_connection)
    });
    match transaction {
        Ok(recipe_id) => Ok(status::Created(format!("/recipe/{}", recipe_id), None)),
        Err(error) => Err(error.to_string()),
    }
}

#[cfg(test)]
mod tests {}
