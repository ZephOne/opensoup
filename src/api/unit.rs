use rocket::http::Status;
use rocket::response::status;

use crate::api::Json;
use crate::db::models::{NewUnit, Unit};
use crate::db::DbConnection;

#[post("/", format = "json", data = "<unit>")]
pub fn create(
    db_connection: DbConnection,
    unit: Json<NewUnit>,
) -> Result<status::Created<String>, String> {
    let new_symbol = &unit.symbol;
    let new_symbol = new_symbol.as_ref();
    match new_symbol {
        Some(symbol) => {
            match NewUnit::create(
                &db_connection,
                NewUnit {
                    name: String::from(&unit.name),
                    symbol: Some(String::from(symbol)),
                },
            ) {
                Ok(unit) => Ok(status::Created(format!("/unit/{}", unit.id), None)),
                Err(error) => Err(error.to_string()),
            }
        }
        None => {
            match NewUnit::create(
                &db_connection,
                NewUnit {
                    name: String::from(&unit.name),
                    symbol: None,
                },
            ) {
                Ok(unit) => Ok(status::Created(format!("/unit/{}", unit.id), None)),
                Err(error) => Err(error.to_string()),
            }
        }
    }
}

#[get("/<id>")]
pub fn read(db_connection: DbConnection, id: i32) -> Result<Json<Unit>, status::NotFound<String>> {
    match Unit::get(&db_connection, id) {
        Ok(unit) => Ok(Json(unit)),
        Err(error) => Err(status::NotFound(error.to_string())),
    }
}

#[put("/<id>", format = "json", data = "<unit>")]
pub fn update(
    db_connection: DbConnection,
    id: i32,
    unit: Json<Unit>,
) -> Result<Json<Unit>, Status> {
    match Unit::get(&db_connection, id) {
        Ok(_) => {
            let new_symbol = &unit.symbol;
            match new_symbol {
                Some(symbol) => {
                    match Unit::update(
                        &db_connection,
                        id,
                        Unit {
                            id: unit.id,
                            name: String::from(&unit.name),
                            symbol: Some(String::from(symbol)),
                        },
                    ) {
                        Ok(updated_unit) => Ok(Json(updated_unit)),
                        Err(_) => Err(Status::InternalServerError),
                    }
                }
                None => {
                    match Unit::update(
                        &db_connection,
                        id,
                        Unit {
                            id: unit.id,
                            name: String::from(&unit.name),
                            symbol: None,
                        },
                    ) {
                        Ok(updated_unit) => Ok(Json(updated_unit)),
                        Err(_) => Err(Status::InternalServerError),
                    }
                }
            }
        }
        Err(_) => Err(Status::Conflict),
    }
}

#[delete("/<id>")]
pub fn delete(db_connection: DbConnection, id: i32) -> Result<(), Status> {
    match Unit::get(&db_connection, id) {
        Ok(_) => match Unit::delete(&db_connection, &id) {
            Ok(_) => Ok(()),
            Err(error) => {
                println!("error: {}", error.to_string());
                Err(Status::InternalServerError)
            }
        },
        Err(_) => Err(Status::NotFound),
    }
}

#[get("/")]
pub fn all(db_connection: DbConnection) -> Json<Vec<Unit>> {
    let units = Unit::all(&db_connection);
    Json(units)
}

#[cfg(test)]
mod tests {
    use diesel::prelude::*;
    use rocket::http::{ContentType, Status};
    use rocket::local::Client;
    use rocket_contrib::json::JsonValue;

    use crate::db::establish_db_connection;
    use crate::db::models::{NewUnit, Unit};
    use crate::db::schema::units::dsl::units as all_unit;
    use crate::opensoup_rocket;

    pub fn teardown() {
        let db_connection = establish_db_connection();
        diesel::delete(all_unit)
            .execute(&db_connection)
            .expect("unable to delete all units");
    }

    #[test]
    fn it_creates_a_unit() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let response = client
            .post("/unit/")
            .body("{\"name\":\"gram\",\"symbol\":\"g\"}")
            .header(ContentType::JSON)
            .dispatch();
        assert_eq!(response.status().code, 201);
        teardown();
    }

    #[test]
    fn it_gets_a_unit_after_it_as_been_created_and_http_status_is_ok() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let db_connection = establish_db_connection();

        let new_unit = NewUnit {
            name: String::from("litre"),
            symbol: Some(String::from("l")),
        };

        if let Ok(created_unit) = NewUnit::create(&db_connection, new_unit) {
            let mut response = client.get(format!("/unit/{}", created_unit.id)).dispatch();
            assert_eq!(response.status(), Status::Ok);
            match created_unit.symbol {
                Some(symbol) => {
                    assert_eq!(
                        response.body_string(),
                        Some(format!(
                            "{{\"id\":{},\"name\":\"{}\",\"symbol\":\"{}\"}}",
                            created_unit.id, created_unit.name, symbol
                        ))
                    );
                }
                None => {
                    assert_eq!(
                        response.body_string(),
                        Some(format!(
                            "{{\"id\":{},\"name\":\"{}\"}}",
                            created_unit.id, created_unit.name
                        ))
                    );
                }
            }
        } else {
            panic!("failed to create unit before getting it!")
        }
        teardown();
    }

    #[test]
    fn it_does_not_get_a_non_existing_unit_and_http_status_is_not_found() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let response = client.get("/unit/1000").dispatch();
        assert_eq!(response.status(), Status::NotFound);
    }

    #[test]
    fn it_updates_a_unit_that_has_been_created_and_status_is_ok() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let db_connection = establish_db_connection();

        let unit = NewUnit {
            name: String::from("litre"),
            symbol: Some(String::from("l")),
        };

        match NewUnit::create(&db_connection, unit) {
            Ok(created_unit) => {
                let new_unit = Unit {
                    id: created_unit.id,
                    name: String::from("litre"),
                    symbol: Some(String::from("L")),
                };

                let response = client
                    .put(format!("/unit/{}", new_unit.id))
                    .body(format!(
                        "{{\"id\":{},\"name\":\"{}\",\"symbol\":\"{}\"}}",
                        new_unit.id,
                        new_unit.name,
                        String::from("L")
                    ))
                    .header(ContentType::JSON)
                    .dispatch();
                assert_eq!(response.status(), Status::Ok);
            }
            Err(error) => println!("error: {}", error.to_string()),
        }
        teardown();
    }

    #[test]
    fn it_does_not_update_a_unit_if_the_unit_does_not_exist_and_status_is_not_found() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");

        let response = client
            .put("/unit/1000")
            .body("{\"id\":1000,\"name\":\"kelvin\"}")
            .header(ContentType::JSON)
            .dispatch();
        assert_eq!(response.status(), Status::Conflict);
    }

    #[test]
    fn it_gets_all_unit_and_status_is_ok() {
        teardown();
        let db_connection = establish_db_connection();

        let unit0 = NewUnit {
            name: String::from("fluid ounce"),
            symbol: Some(String::from("fl oz")),
        };
        let unit1 = NewUnit {
            name: String::from("cups"),
            symbol: None,
        };

        let units = vec![unit0, unit1];
        let mut created_units: Vec<Unit> = Vec::new();

        for unit in units {
            if let Ok(new_unit) = NewUnit::create(&db_connection, unit) {
                created_units.push(new_unit);
            } else {
                panic!("fail to insert new unit");
            }
        }

        created_units.reverse();
        println!("created_units size: {}", created_units.len());

        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let mut response = client.get("/unit/").dispatch();
        if let Some(body) = response.body_string() {
            if let Ok(json_created_units) = serde_json::from_str::<JsonValue>(&body) {
                assert_eq!(json_created_units, json!(created_units));
                assert_eq!(response.status(), Status::Ok);
            } else {
                panic!("Failed to parse json from response body.");
            }
        } else {
            panic!("Failed to get body from response.");
        }
        teardown();
    }

    #[test]
    fn it_deletes_a_unit_which_has_just_been_created_and_status_is_ok() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let db_connection = establish_db_connection();

        let unit = NewUnit {
            name: String::from("cups"),
            symbol: None,
        };

        if let Ok(created_unit) = NewUnit::create(&db_connection, unit) {
            let response = client
                .delete(format!("/unit/{}", created_unit.id))
                .dispatch();
            assert_eq!(response.status(), Status::Ok);
        }
        teardown();
    }

    #[test]
    fn it_cannot_delete_a_non_existing_unit_and_status_is_internal_server_error() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let response = client.delete("/unit/1000").dispatch();
        assert_eq!(response.status(), Status::NotFound);
    }
}
