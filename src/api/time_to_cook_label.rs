use rocket::http::Status;
use rocket::response::status;

use crate::api::Json;
use crate::db::models::{NewTimeToCookLabel, TimeToCookLabel};
use crate::db::DbConnection;

#[post("/", format = "json", data = "<time_to_cook_label>")]
pub fn create(
    db_connection: DbConnection,
    time_to_cook_label: Json<NewTimeToCookLabel>,
) -> Result<status::Created<String>, String> {
    let new_time_to_cook_label = NewTimeToCookLabel {
        name: String::from(&time_to_cook_label.name),
    };
    match NewTimeToCookLabel::create(&db_connection, new_time_to_cook_label) {
        Ok(created_time_to_cook_label) => Ok(status::Created(
            format!("/time_to_cook_label/{}", created_time_to_cook_label.id),
            None,
        )),
        Err(error) => Err(error.to_string()),
    }
}

#[get("/<id>")]
pub fn read(
    db_connection: DbConnection,
    id: i32,
) -> Result<Json<TimeToCookLabel>, status::NotFound<String>> {
    match TimeToCookLabel::get(&db_connection, id) {
        Ok(time_to_cook_label) => Ok(Json(time_to_cook_label)),
        Err(error) => Err(status::NotFound(error.to_string())),
    }
}

#[put("/<id>", format = "json", data = "<time_to_cook_label>")]
pub fn update(
    db_connection: DbConnection,
    id: i32,
    time_to_cook_label: Json<TimeToCookLabel>,
) -> Result<Json<TimeToCookLabel>, Status> {
    match TimeToCookLabel::get(&db_connection, id) {
        Ok(_) => {
            let new_time_to_cook_label = TimeToCookLabel {
                id: time_to_cook_label.id,
                name: String::from(&time_to_cook_label.name),
            };

            match TimeToCookLabel::update(&db_connection, id, new_time_to_cook_label) {
                Ok(updated_time_to_cook_label) => Ok(Json(updated_time_to_cook_label)),
                Err(_) => Err(Status::InternalServerError),
            }
        }
        Err(_) => Err(Status::Conflict),
    }
}

#[delete("/<id>")]
pub fn delete(db_connection: DbConnection, id: i32) -> Result<(), Status> {
    match TimeToCookLabel::get(&db_connection, id) {
        Ok(_) => match TimeToCookLabel::delete(&db_connection, &id) {
            Ok(_) => Ok(()),
            Err(error) => {
                println!("error: {}", error.to_string());
                Err(Status::InternalServerError)
            }
        },
        Err(_) => Err(Status::NotFound),
    }
}

#[get("/")]
pub fn all(db_connection: DbConnection) -> Json<Vec<TimeToCookLabel>> {
    let time_to_cook_labels = TimeToCookLabel::all(&db_connection);
    Json(time_to_cook_labels)
}

#[cfg(test)]
mod tests {
    use diesel::prelude::*;
    use rocket::http::{ContentType, Status};
    use rocket::local::Client;
    use rocket_contrib::json::JsonValue;

    use crate::db::establish_db_connection;
    use crate::db::models::{NewTimeToCookLabel, TimeToCookLabel};
    use crate::db::schema::time_to_cook_labels::dsl::time_to_cook_labels as all_time_to_cook_label;
    use crate::opensoup_rocket;

    pub fn teardown() {
        let db_connection = establish_db_connection();
        diesel::delete(all_time_to_cook_label)
            .execute(&db_connection)
            .expect("unable to delete all time_to_cook_labels");
    }

    #[test]
    fn it_creates_a_time_to_cook_label() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let response = client
            .post("/time_to_cook_label/")
            .body("{\"name\":\"very long\"}")
            .header(ContentType::JSON)
            .dispatch();
        assert_eq!(response.status().code, 201);
        teardown();
    }

    #[test]
    fn it_gets_a_time_to_cook_label_after_it_as_been_created_and_http_status_is_ok() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let db_connection = establish_db_connection();

        let new_time_to_cook_label = NewTimeToCookLabel {
            name: String::from("strawberry"),
        };

        if let Ok(created_time_to_cook_label) =
            NewTimeToCookLabel::create(&db_connection, new_time_to_cook_label)
        {
            let mut response = client
                .get(format!(
                    "/time_to_cook_label/{}",
                    created_time_to_cook_label.id
                ))
                .dispatch();
            assert_eq!(response.status(), Status::Ok);
            assert_eq!(
                response.body_string(),
                Some(format!(
                    "{{\"id\":{},\"name\":\"{}\"}}",
                    created_time_to_cook_label.id, created_time_to_cook_label.name
                ))
            );
        } else {
            panic!("failed to create time_to_cook_label before getting it!")
        }
        teardown();
    }

    #[test]
    fn it_does_not_get_a_non_existing_time_to_cook_label_and_http_status_is_not_found() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let response = client.get("/time_to_cook_label/1000").dispatch();
        assert_eq!(response.status(), Status::NotFound);
    }

    #[test]
    fn it_updates_a_time_to_cook_label_that_has_been_created_and_status_is_ok() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let db_connection = establish_db_connection();

        let time_to_cook_label = NewTimeToCookLabel {
            name: String::from("potato"),
        };

        match NewTimeToCookLabel::create(&db_connection, time_to_cook_label) {
            Ok(created_time_to_cook_label) => {
                let new_time_to_cook_label = TimeToCookLabel {
                    id: created_time_to_cook_label.id,
                    name: String::from("sweet potato"),
                };

                let response = client
                    .put(format!("/time_to_cook_label/{}", new_time_to_cook_label.id))
                    .body(format!(
                        "{{\"id\":{},\"name\":\"{}\"}}",
                        new_time_to_cook_label.id, new_time_to_cook_label.name
                    ))
                    .header(ContentType::JSON)
                    .dispatch();
                assert_eq!(response.status(), Status::Ok);
            }
            Err(error) => println!("error: {}", error.to_string()),
        }
        teardown();
    }

    #[test]
    fn it_does_not_update_a_time_to_cook_label_if_the_time_to_cook_label_does_not_exist_and_status_is_not_found(
    ) {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");

        let response = client
            .put("/time_to_cook_label/1000")
            .body("{\"id\":1000,\"name\":\"very long\"}")
            .header(ContentType::JSON)
            .dispatch();
        assert_eq!(response.status(), Status::Conflict);
    }

    #[test]
    fn it_gets_all_time_to_cook_label_and_status_is_ok() {
        teardown();
        let db_connection = establish_db_connection();

        let time_to_cook_label0 = NewTimeToCookLabel {
            name: String::from("long"),
        };
        let time_to_cook_label1 = NewTimeToCookLabel {
            name: String::from("fast"),
        };

        let time_to_cook_labels = vec![time_to_cook_label0, time_to_cook_label1];
        let mut created_time_to_cook_labels: Vec<TimeToCookLabel> = Vec::new();

        for time_to_cook_label in time_to_cook_labels {
            if let Ok(new_time_to_cook_label) =
                NewTimeToCookLabel::create(&db_connection, time_to_cook_label)
            {
                created_time_to_cook_labels.push(new_time_to_cook_label);
            } else {
                panic!("fail to insert new time_to_cook_label");
            }
        }

        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let mut response = client.get("/time_to_cook_label/").dispatch();
        if let Some(body) = response.body_string() {
            if let Ok(json_created_time_to_cook_labels) = serde_json::from_str::<JsonValue>(&body) {
                assert_eq!(
                    json_created_time_to_cook_labels,
                    json!(created_time_to_cook_labels)
                );
                assert_eq!(response.status(), Status::Ok);
            } else {
                panic!("Failed to parse json from response body.");
            }
        } else {
            panic!("Failed to get body from response.");
        }
        teardown();
    }

    #[test]
    fn it_deletes_a_time_to_cook_label_which_has_just_been_created_and_status_is_ok() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let db_connection = establish_db_connection();

        let time_to_cook_label = NewTimeToCookLabel {
            name: String::from("blackberry"),
        };

        if let Ok(created_time_to_cook_label) =
            NewTimeToCookLabel::create(&db_connection, time_to_cook_label)
        {
            let response = client
                .delete(format!(
                    "/time_to_cook_label/{}",
                    created_time_to_cook_label.id
                ))
                .dispatch();
            assert_eq!(response.status(), Status::Ok);
        }
        teardown();
    }

    #[test]
    fn it_cannot_delete_a_non_existing_time_to_cook_label_and_status_is_internal_server_error() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let response = client.delete("/time_to_cook_label/1000").dispatch();
        assert_eq!(response.status(), Status::NotFound);
    }
}
