use rocket_contrib::json::Json;

pub mod complete_recipe;
pub mod cook;
pub mod ingredient;
pub mod recipe;
pub mod role;
pub mod time_to_cook_label;
pub mod unit;
