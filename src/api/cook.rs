use rocket::http::Status;
use rocket::response::status;

use crate::api::Json;
use crate::db::models::{Cook, CookRole, NewCook, Recipe, RecipeCook, Role};
use crate::db::DbConnection;

#[post("/", format = "json", data = "<cook>")]
pub fn create(
    db_connection: DbConnection,
    cook: Json<NewCook>,
) -> Result<status::Created<String>, String> {
    let new_cook = NewCook {
        email: String::from(&cook.email),
        name: String::from(&cook.name),
        password: String::from(&cook.password),
    };
    match NewCook::create(&db_connection, new_cook) {
        Ok(created_cook) => Ok(status::Created(format!("/cook/{}", created_cook.id), None)),
        Err(error) => Err(error.to_string()),
    }
}

#[get("/<id>")]
pub fn read(db_connection: DbConnection, id: i32) -> Result<Json<Cook>, status::NotFound<String>> {
    match Cook::get(&db_connection, id) {
        Ok(cook) => Ok(Json(cook)),
        Err(error) => Err(status::NotFound(error.to_string())),
    }
}

#[put("/<id>", format = "json", data = "<cook>")]
pub fn update(
    db_connection: DbConnection,
    id: i32,
    cook: Json<Cook>,
) -> Result<Json<Cook>, Status> {
    match Cook::get(&db_connection, id) {
        Ok(_) => {
            let new_cook = Cook {
                id: cook.id,
                email: String::from(&cook.email),
                name: String::from(&cook.name),
                password: String::from(&cook.password),
            };

            match Cook::update(&db_connection, id, new_cook) {
                Ok(updated_cook) => Ok(Json(updated_cook)),
                Err(_) => Err(Status::InternalServerError),
            }
        }
        Err(_) => Err(Status::Conflict),
    }
}

#[delete("/<id>")]
pub fn delete(db_connection: DbConnection, id: i32) -> Result<(), Status> {
    match Cook::get(&db_connection, id) {
        Ok(_) => match Cook::delete(&db_connection, &id) {
            Ok(_) => Ok(()),
            Err(error) => {
                println!("error: {}", error.to_string());
                Err(Status::InternalServerError)
            }
        },
        Err(_) => Err(Status::NotFound),
    }
}

#[get("/")]
pub fn all(db_connection: DbConnection) -> Json<Vec<Cook>> {
    let cooks = Cook::all(&db_connection);
    Json(cooks)
}

#[get("/<cook_id>/recipe")]
pub fn all_recipes_by_cook(db_connection: DbConnection, cook_id: i32) -> Json<Vec<Recipe>> {
    let recipes = Recipe::all_by_cook(&db_connection, cook_id);
    Json(recipes)
}

#[get("/<cook_id>/usage")]
pub fn all_recipe_usage_by_cook(
    db_connection: DbConnection,
    cook_id: i32,
) -> Result<Json<Vec<RecipeCook>>, status::NotFound<String>> {
    match RecipeCook::all_recipe_usage_by_cook(&db_connection, cook_id) {
        Ok(all_recipes_usage) => Ok(Json(all_recipes_usage)),
        Err(error) => Err(status::NotFound(error.to_string())),
    }
}

#[get("/<cook_id>/usage/recipe/<recipe_id>")]
pub fn recipe_usage_by_cook(
    db_connection: DbConnection,
    cook_id: i32,
    recipe_id: i32,
) -> Result<Json<Vec<RecipeCook>>, status::NotFound<String>> {
    match RecipeCook::recipe_usage_by_cook(&db_connection, cook_id, recipe_id) {
        Ok(recipe_usage) => Ok(Json(recipe_usage)),
        Err(error) => Err(status::NotFound(error.to_string())),
    }
}

#[get("/<cook_id>/role")]
pub fn all_role_by_cook(
    db_connection: DbConnection,
    cook_id: i32,
) -> Result<Json<Vec<Role>>, status::NotFound<String>> {
    match CookRole::all_role_by_cook(&db_connection, cook_id) {
        Ok(roles) => Ok(Json(roles)),
        Err(error) => Err(status::NotFound(error.to_string())),
    }
}

#[cfg(test)]
mod tests {

    use chrono::NaiveDate;
    use diesel::prelude::*;
    use rocket::http::{ContentType, Status};
    use rocket::local::Client;
    use rocket_contrib::json::JsonValue;

    use crate::db::establish_db_connection;
    use crate::db::models::{
        Cook, NewCook, NewCookRole, NewRecipe, NewRecipeCook, NewRole, Recipe, Role,
        TimeToCookLabel,
    };
    use crate::db::schema::cooks::dsl::cooks as all_cook;
    use crate::db::schema::recipes::dsl::recipes as all_recipe;
    use crate::db::schema::time_to_cook_labels;
    use crate::db::schema::time_to_cook_labels::dsl::time_to_cook_labels as all_time_to_cook_label;
    use crate::opensoup_rocket;

    pub fn teardown() {
        let db_connection = establish_db_connection();
        diesel::delete(all_time_to_cook_label)
            .execute(&db_connection)
            .expect("unable to delete all time_to_cook_label");
        diesel::delete(all_recipe)
            .execute(&db_connection)
            .expect("unable to delete all recipe");
        diesel::delete(all_cook)
            .execute(&db_connection)
            .expect("unable to delete all cooks");
    }

    #[test]
    fn it_creates_a_cook() {
        let client = Client::new(opensoup_rocket()).expect("Valid rocket instance");
        let response = client
            .post("/cook/")
            .body("{\"email\":\"lucy@lsd.fr\",\"name\":\"Lucy Miller\",\"password\":\"lsd\"}")
            .header(ContentType::JSON)
            .dispatch();
        assert_eq!(response.status().code, 201);
        teardown();
    }

    #[test]
    fn it_cannot_create_a_cook() {
        let client = Client::new(opensoup_rocket()).expect("Valid rocket instance");
        let response = client
            .post("/cook/")
            .body("{\"name\":\"Lucy Miller\",\"password\":\"lsd\"}")
            .header(ContentType::JSON)
            .dispatch();
        assert_eq!(response.status(), Status::UnprocessableEntity);
    }

    #[test]
    fn it_gets_a_cook_after_it_as_been_created_and_http_status_is_ok() {
        teardown();
        let client = Client::new(opensoup_rocket()).expect("Valid rocket instance");
        let db_connection = establish_db_connection();

        let new_cook = NewCook {
            email: String::from("peter.pan@neverland.com"),
            name: String::from("Peter Pan"),
            password: String::from("nevergrowup"),
        };

        if let Ok(created_cook) = NewCook::create(&db_connection, new_cook) {
            let mut response = client.get(format!("/cook/{}", created_cook.id)).dispatch();
            assert_eq!(response.status(), Status::Ok);
            assert_eq!(
                response.body_string(),
                Some(format!(
                    "{{\"id\":{},\"email\":\"{}\",\"name\":\"{}\",\"password\":\"{}\"}}",
                    created_cook.id, created_cook.email, created_cook.name, created_cook.password
                ))
            );
        } else {
            panic!("failed to create cook before getting it!")
        }
        teardown();
    }

    #[test]
    fn it_does_not_get_a_non_existing_cook_and_http_status_is_not_found() {
        let client = Client::new(opensoup_rocket()).expect("Valid rocket instance");
        let response = client.get("/cook/1000").dispatch();
        assert_eq!(response.status(), Status::NotFound);
    }

    #[test]
    fn it_updates_a_cook_that_has_been_created_and_status_is_ok() {
        teardown();
        let client = Client::new(opensoup_rocket()).expect("Valid rocket instance");
        let db_connection = establish_db_connection();

        let cook = NewCook {
            email: String::from("peter.pan@neverland.com"),
            name: String::from("Peter Pan"),
            password: String::from("nevergrowup"),
        };

        if let Ok(created_cook) = NewCook::create(&db_connection, cook) {
            let new_cook = Cook {
                id: created_cook.id,
                email: String::from("peter.banning@grownup.com"),
                name: String::from("Peter Banning"),
                password: String::from("hasgrownup"),
            };

            let response = client
                .put(format!("/cook/{}", new_cook.id))
                .body(format!(
                    "{{\"id\":{},\"email\":\"{}\",\"name\":\"{}\",\"password\":\"{}\"}}",
                    new_cook.id, new_cook.email, new_cook.name, new_cook.password
                ))
                .header(ContentType::JSON)
                .dispatch();
            assert_eq!(response.status(), Status::Ok);
        } else {
            panic!("Failed to insert new cook before updating it.");
        }
        teardown();
    }

    #[test]
    fn it_does_not_update_a_cook_if_the_cook_does_not_exist_and_status_is_not_found() {
        let client = Client::new(opensoup_rocket()).expect("Valid rocket instance");

        let response = client
                .put("/cook/1000")
                .body(
                    "{\"id\":1000,\"email\":\"mrnobody@uno.com\",\"name\":\"nobody\",\"password\":\"nopassword\"}"
                )
                .header(ContentType::JSON)
                .dispatch();
        assert_eq!(response.status(), Status::Conflict);
    }

    #[test]
    fn it_gets_all_cook_and_status_is_ok() {
        teardown();
        let db_connection = establish_db_connection();

        let cook0 = NewCook {
            email: String::from("cook0@cuisine.com"),
            name: String::from("John Doe"),
            password: String::from("cook0pwd"),
        };
        let cook1 = NewCook {
            email: String::from("cook1@cuisine.com"),
            name: String::from("Jack Slave"),
            password: String::from("cook1pwd"),
        };
        let cook2 = NewCook {
            email: String::from("cook2@cuisine.com"),
            name: String::from("Henry the pilot"),
            password: String::from("cook2pwd"),
        };
        let cook3 = NewCook {
            email: String::from("cook3@cuisine.com"),
            name: String::from("Sanji Vinsmoke"),
            password: String::from("cook3pwd"),
        };
        let cook4 = NewCook {
            email: String::from("cook4@cuisine.com"),
            name: String::from("King Arthur"),
            password: String::from("cook4pwd"),
        };

        let cooks = vec![cook0, cook1, cook2, cook3, cook4];
        let mut created_cooks: Vec<Cook> = Vec::new();

        for cook in cooks {
            if let Ok(new_cook) = NewCook::create(&db_connection, cook) {
                created_cooks.push(new_cook);
            } else {
                panic!("fail to insert new cook");
            }
        }

        created_cooks.reverse();
        println!("created_cooks size: {}", created_cooks.len());

        let client = Client::new(opensoup_rocket()).expect("Valid rocket instance");
        let mut response = client.get("/cook/").dispatch();
        if let Some(body) = response.body_string() {
            if let Ok(json_created_cooks) = serde_json::from_str::<JsonValue>(&body) {
                assert_eq!(json_created_cooks, json!(created_cooks));
                assert_eq!(response.status(), Status::Ok);
            } else {
                panic!("Failed to parse json from response body.");
            }
        } else {
            panic!("Failed to get body from response.");
        }
    }

    #[test]
    fn it_deletes_a_cook_which_has_just_been_created_and_status_is_ok() {
        let client = Client::new(opensoup_rocket()).expect("Valid rocket instance");
        let db_connection = establish_db_connection();

        let cook = NewCook {
            email: String::from("peter.pan@neverland.com"),
            name: String::from("Peter Pan"),
            password: String::from("nevergrowup"),
        };

        if let Ok(created_cook) = NewCook::create(&db_connection, cook) {
            let response = client
                .delete(format!("/cook/{}", created_cook.id))
                .dispatch();
            assert_eq!(response.status(), Status::Ok);
        }
    }

    #[test]
    fn it_cannot_delete_a_non_existing_user_and_status_is_internal_server_error() {
        let client = Client::new(opensoup_rocket()).expect("Valid rocket instance");
        let response = client.delete("/cook/1000").dispatch();
        assert_eq!(response.status(), Status::NotFound);
    }

    #[test]
    fn it_gets_all_recipes_for_a_given_cook_and_status_is_ok() {
        teardown();
        let db_connection = establish_db_connection();

        let cook = NewCook {
            email: String::from("peter.pan@neverland.com"),
            name: String::from("Peter Pan"),
            password: String::from("nevergrowup"),
        };

        let names = ["fast", "medium", "long"];

        for name in names.iter() {
            diesel::insert_into(all_time_to_cook_label)
                .values(time_to_cook_labels::name.eq(name))
                .execute(&db_connection)
                .expect("Error inserting new time_to_cook_label");
        }

        let time_to_cook_label_fast =
            TimeToCookLabel::get_by_name(&db_connection, String::from("fast"))
                .expect("Failed to create time to cook label");
        let time_to_cook_label_medium =
            TimeToCookLabel::get_by_name(&db_connection, String::from("medium"))
                .expect("Failed to create time to cook label");
        let time_to_cook_label_long =
            TimeToCookLabel::get_by_name(&db_connection, String::from("long"))
                .expect("Failed to create time to cook label");

        let created_cook = NewCook::create(&db_connection, cook).unwrap();
        let recipe1 = NewRecipe {
            name: String::from("tartines chèvre et poireaux"),
            time_to_cook_label_id: Some(time_to_cook_label_fast.id),
            content: String::from(
                "Lavez et émincez finement les blancs de poireaux.
Faites revenir les poireaux et les lardons dans une sauteuse.
Assaisonnez.
Toastez les tranches de pains.
Tartinez les de fromage de chèvre.
Garnissez les avec les poireaux et lardons.",
            ),
            cook_id: created_cook.id,
        };

        let recipe2 = NewRecipe {
                name: String::from("lasagnes"),
                time_to_cook_label_id: Some(time_to_cook_label_long.id),
            content: String::from(
"# Préparation de la bolognaise.
Coupez l'oignon en petits morceaux et faites les revenir dans de l'huile d'olive.
Quand les oignons ont bien bruni, ajoutez la viande haché.
Faites cuire à feu moyen puis ajouter la sauce tomate.
Préchauffez le four à 180°C.

# Préparation de la béchamel
Faites fondre le beurre à feu vif.
Une fois fondu, rajoutez la farine puis remuez avec un fouet à feu moyen.
Quand le mélange est homogène rajoutez progressivement le lait sans arrêter de fouetter pour éviter les grumeaux.
Continuez de remuer jusqu'à ce que la béchamel s'épaississe.

# Préparation des feuilles de lasagne
Portez ébullition l'eau afin de pouvoir.
Baignez les feuilles de lasagne jusqu'à qu'elles soient ramollies
Sortez les de l'eau.

# Assemblage
Placez des feuilles de lasagne jusqu'à couvrir la surface de votre plât.
Répartissez une couche bolognaise sur les feuilles de lasagnes.
Faites de même avec la béchamel.
Placez également des tranches de mozzarella.
Recommencez ces quatre opérations deux fois.
Placez ensuite une dernière couche de béchamel.
Saupoudrez ensuite de parmesan.

# Cuisson
Placez au four pendant 45 minutes."
            ),
            cook_id: created_cook.id,
        };

        let recipe3 = NewRecipe {
                name: String::from("pancakes"),
                time_to_cook_label_id: Some(time_to_cook_label_medium.id),
                content: String::from(
"# Préparation de la pâte.
Faites fondre le beurre.
Pendant ce temps, mélangez dans un saladier la farine, le sucre et la levure et formez un puit.
Ajoutez les œufs dans le puit et fouttez.
Versez ensuite le beurre fondu.
Délayez ensuite progressivement le mélange avec le lait afin d'éviter les grumeaux.
Laissez reposer la pâte au minimum une heure.

# Cuisson des pancakes
Chauffez à feu moyen une poêle légèrement huilée.
Déposez une demi louche de pâte à pancake dans la poêle et laissez cuire jusqu'à que des trous se forment sur la surface du pancake.
Retournez le pancake et laissez le cuire environ 45 secondes.
Retirez le pancake.
Répétez ces opérations jusqu'à ne plus avoir de pâte."
                ),
                cook_id: created_cook.id,
            };

        let recipe4 = NewRecipe {
                name: String::from("quiche lorraine"),
                time_to_cook_label_id: Some(time_to_cook_label_long.id),
                content: String::from(
"Préchauffez le four à 180°C.
Battez les œufs comme une omelette avec le sel, le poivre, le lait et la crème fraîche dans un saladier.
Faites blanchir les oignons.
Faites revenir les lardons avec les oignons.
Étalez la pâte brisée et piquez sur toute la surface avec une fourchette.
Versez le contenu du saladier sur la pâte brisée et répartissez les oignons et lardons dessus.
Placez au four pendant 45 à 50 minutes."
                ),
                cook_id: created_cook.id,
            };

        let mut created_recipes: Vec<Recipe> = vec![];

        let new_recipes_with_dates = vec![
            (
                recipe1,
                NaiveDate::from_ymd(2019, 5, 26).and_hms(16, 15, 20),
                None,
            ),
            (
                recipe2,
                NaiveDate::from_ymd(2019, 4, 3).and_hms(9, 10, 11),
                None,
            ),
            (
                recipe3,
                NaiveDate::from_ymd(2019, 2, 24).and_hms(9, 45, 53),
                Some(NaiveDate::from_ymd(2019, 8, 31).and_hms(12, 50, 4)),
            ),
            (
                recipe4,
                NaiveDate::from_ymd(2019, 1, 18).and_hms(16, 15, 20),
                Some(NaiveDate::from_ymd(2019, 5, 27).and_hms(14, 24, 3)),
            ),
        ];

        for recipe_with_dates in new_recipes_with_dates {
            if let Ok(created_recipe) = NewRecipe::create(&db_connection, recipe_with_dates.0) {
                let created_recipe = Recipe::update(
                    &db_connection,
                    Recipe {
                        id: created_recipe.id,
                        name: created_recipe.name,
                        time_to_cook_label_id: created_recipe.time_to_cook_label_id,
                        content: created_recipe.content,
                        creation_date: recipe_with_dates.1,
                        update_date: recipe_with_dates.2,
                        cook_id: created_recipe.cook_id,
                    },
                )
                .expect("Failed to update recipe");
                created_recipes.push(created_recipe);
            } else {
                panic!("Failed to insert recipe");
            }
        }

        let client = Client::new(opensoup_rocket()).expect("Valid rocket instance");
        let mut response = client
            .get(format!("/cook/{}/recipe/", created_cook.id))
            .dispatch();
        if let Some(body) = response.body_string() {
            if let Ok(json_created_recipes) = serde_json::from_str::<JsonValue>(&body) {
                assert_eq!(json_created_recipes, json!(created_recipes));
                assert_eq!(response.status(), Status::Ok);
            } else {
                panic!("Failed to parse json from response body.");
            }
        } else {
            panic!("Failed to get body from response.");
        }
    }

    #[test]
    fn it_get_all_recipes_usage_by_cook_and_status_is_ok() {
        teardown();
        let db_connection = establish_db_connection();

        let cook = NewCook {
            email: String::from("peter.pan@neverland.com"),
            name: String::from("Peter Pan"),
            password: String::from("nevergrowup"),
        };

        let created_cook = NewCook::create(&db_connection, cook).unwrap();

        let names = ["fast", "medium", "long"];

        for name in names.iter() {
            diesel::insert_into(all_time_to_cook_label)
                .values(time_to_cook_labels::name.eq(name))
                .execute(&db_connection)
                .expect("Error inserting new time_to_cook_label");
        }

        let time_to_cook_label_fast =
            TimeToCookLabel::get_by_name(&db_connection, String::from("fast"))
                .expect("Failed to create time to cook label");
        let time_to_cook_label_medium =
            TimeToCookLabel::get_by_name(&db_connection, String::from("medium"))
                .expect("Failed to create time to cook label");
        let time_to_cook_label_long =
            TimeToCookLabel::get_by_name(&db_connection, String::from("long"))
                .expect("Failed to create time to cook label");

        let insertable_recipe1 = NewRecipe {
                name: String::from("pancakes"),
                time_to_cook_label_id: Some(time_to_cook_label_medium.id),
                content: String::from(
"# Préparation de la pâte.
Faites fondre le beurre.
Pendant ce temps, mélangez dans un saladier la farine, le sucre et la levure et formez un puit.
Ajoutez les œufs dans le puit et fouttez.
Versez ensuite le beurre fondu.
Délayez ensuite progressivement le mélange avec le lait afin d'éviter les grumeaux.
Laissez reposer la pâte au minimum une heure.

# Cuisson des pancakes
Chauffez à feu moyen une poêle légèrement huilée.
Déposez une demi louche de pâte à pancake dans la poêle et laissez cuire jusqu'à que des trous se forment sur la surface du pancake.
Retournez le pancake et laissez le cuire environ 45 secondes.
Retirez le pancake.
Répétez ces opérations jusqu'à ne plus avoir de pâte."
                ),


                cook_id: created_cook.id,
            };
        let recipe1 =
            NewRecipe::create(&db_connection, insertable_recipe1).expect("Failed to create recipe");
        let recipe1 = Recipe::update(
            &db_connection,
            Recipe {
                id: recipe1.id,
                name: recipe1.name,
                time_to_cook_label_id: recipe1.time_to_cook_label_id,
                content: recipe1.content,
                creation_date: NaiveDate::from_ymd(2019, 2, 24).and_hms(9, 45, 53),
                update_date: Some(NaiveDate::from_ymd(2019, 8, 31).and_hms(12, 50, 4)),
                cook_id: recipe1.cook_id,
            },
        )
        .expect("Failed to update recipe");

        let insertable_recipe2 = NewRecipe {
            name: String::from("quiche lorraine"),
            time_to_cook_label_id: Some(time_to_cook_label_long.id),
            content: String::from(
"Préchauffez le four à 180°C.
Battez les œufs comme une omelette avec le sel, le poivre, le lait et la crème fraîche dans un saladier.
Faites blanchir les oignons.
Faites revenir les lardons avec les oignons.
Étalez la pâte brisée et piquez sur toute la surface avec une fourchette.
Versez le contenu du saladier sur la pâte brisée et répartissez les oignons et lardons dessus.
Placez au four pendant 45 à 50 minutes."
            ),


            cook_id: created_cook.id,
        };
        let recipe2 =
            NewRecipe::create(&db_connection, insertable_recipe2).expect("Failed to create recipe");
        let recipe2 = Recipe::update(
            &db_connection,
            Recipe {
                id: recipe2.id,
                name: recipe2.name,
                time_to_cook_label_id: recipe2.time_to_cook_label_id,
                content: recipe2.content,
                creation_date: NaiveDate::from_ymd(2019, 1, 18).and_hms(16, 15, 20),
                update_date: Some(NaiveDate::from_ymd(2019, 5, 27).and_hms(14, 24, 3)),
                cook_id: recipe2.cook_id,
            },
        )
        .expect("Failed to update recipe");

        let insertable_recipe3 = NewRecipe {
            name: String::from("tartines chèvre et poireaux"),
            time_to_cook_label_id: Some(time_to_cook_label_fast.id),
            content: String::from(
                "Lavez et émincez finement les blancs de poireaux.
Faites revenir les poireaux et les lardons dans une sauteuse.
Assaisonnez.
Toastez les tranches de pains.
Tartinez les de fromage de chèvre.
Garnissez les avec les poireaux et lardons.",
            ),
            cook_id: created_cook.id,
        };
        let recipe3 =
            NewRecipe::create(&db_connection, insertable_recipe3).expect("Failed to create recipe");
        let recipe3 = Recipe::update(
            &db_connection,
            Recipe {
                id: recipe3.id,
                name: recipe3.name,
                time_to_cook_label_id: recipe3.time_to_cook_label_id,
                content: recipe3.content,
                creation_date: NaiveDate::from_ymd(2019, 5, 26).and_hms(16, 15, 20),
                update_date: None,
                cook_id: recipe3.cook_id,
            },
        )
        .expect("Failed to update recipe");

        let dates = vec![
            NaiveDate::from_ymd(2020, 4, 8),
            NaiveDate::from_ymd(2020, 6, 5),
            NaiveDate::from_ymd(2019, 7, 13),
            NaiveDate::from_ymd(2020, 1, 17),
            NaiveDate::from_ymd(2020, 2, 22),
            NaiveDate::from_ymd(2019, 8, 6),
            NaiveDate::from_ymd(2020, 1, 13),
            NaiveDate::from_ymd(2020, 3, 9),
            NaiveDate::from_ymd(2019, 6, 30),
            NaiveDate::from_ymd(2019, 12, 3),
            NaiveDate::from_ymd(2019, 9, 8),
            NaiveDate::from_ymd(2020, 6, 11),
            NaiveDate::from_ymd(2019, 7, 17),
            NaiveDate::from_ymd(2019, 10, 13),
        ];

        let mut created_recipe_cook_usage = Vec::new();

        for (index, use_date) in dates.iter().enumerate() {
            let recipe;
            match index {
                0..=8 => recipe = &recipe1,
                9..=11 => recipe = &recipe2,
                _ => recipe = &recipe3,
            }
            if let Ok(new_recipe_cook_usage) = NewRecipeCook::create(
                &db_connection,
                NewRecipeCook {
                    use_date: *use_date,
                    recipe_id: recipe.id,
                    cook_id: created_cook.id,
                },
            ) {
                created_recipe_cook_usage.push(new_recipe_cook_usage);
            } else {
                panic!("Failed to insert new recipe usage");
            }
        }

        let client = Client::new(opensoup_rocket()).expect("Valid rocket instance");
        let mut response = client
            .get(format!("/cook/{}/usage/", created_cook.id))
            .dispatch();
        if let Some(body) = response.body_string() {
            if let Ok(json_created_recipe_usage) = serde_json::from_str::<JsonValue>(&body) {
                assert_eq!(json_created_recipe_usage, json!(created_recipe_cook_usage));
                assert_eq!(response.status(), Status::Ok);
            } else {
                panic!("Failed to parse json from response body.");
            }
        } else {
            panic!("Failed to get body from response.");
        }
    }

    #[test]
    fn it_get_a_recipe_usage_for_a_cook_and_status_is_ok() {
        teardown();
        let db_connection = establish_db_connection();

        let insertable_cook = NewCook {
            email: String::from("peter.pan@neverland.com"),
            name: String::from("Peter Pan"),
            password: String::from("nevergrowup"),
        };

        let cook = NewCook::create(&db_connection, insertable_cook).unwrap();

        diesel::insert_into(all_time_to_cook_label)
            .values(time_to_cook_labels::name.eq("long"))
            .execute(&db_connection)
            .expect("Error inserting new time_to_cook_label");

        let time_to_cook_label_long =
            TimeToCookLabel::get_by_name(&db_connection, String::from("long"))
                .expect("Failed to create time to cook label");

        let insertable_recipe = NewRecipe {
            name: String::from("quiche lorraine"),
            time_to_cook_label_id: Some(time_to_cook_label_long.id),
            content: String::from(
"Préchauffez le four à 180°C.
Battez les œufs comme une omelette avec le sel, le poivre, le lait et la crème fraîche dans un saladier.
Faites blanchir les oignons.
Faites revenir les lardons avec les oignons.
Étalez la pâte brisée et piquez sur toute la surface avec une fourchette.
Versez le contenu du saladier sur la pâte brisée et répartissez les oignons et lardons dessus.
Placez au four pendant 45 à 50 minutes."
            ),
            cook_id: cook.id,
        };

        let recipe = NewRecipe::create(&db_connection, insertable_recipe).unwrap();

        let recipe = Recipe::update(
            &db_connection,
            Recipe {
                id: recipe.id,
                name: recipe.name,
                time_to_cook_label_id: recipe.time_to_cook_label_id,
                content: recipe.content,
                creation_date: NaiveDate::from_ymd(2019, 1, 18).and_hms(16, 15, 20),
                update_date: Some(NaiveDate::from_ymd(2019, 5, 27).and_hms(14, 24, 3)),
                cook_id: recipe.cook_id,
            },
        )
        .expect("Failed to update recipe");

        let dates = vec![
            NaiveDate::from_ymd(2020, 4, 8),
            NaiveDate::from_ymd(2020, 6, 5),
            NaiveDate::from_ymd(2019, 7, 13),
            NaiveDate::from_ymd(2020, 1, 17),
            NaiveDate::from_ymd(2019, 10, 13),
        ];

        let mut created_recipe_cook_usage = Vec::new();

        for use_date in dates {
            match NewRecipeCook::create(
                &db_connection,
                NewRecipeCook {
                    use_date,
                    recipe_id: recipe.id,
                    cook_id: cook.id,
                },
            ) {
                Err(_) => panic!("Failed to insert new recipe usage"),
                Ok(recipe_usage) => created_recipe_cook_usage.push(recipe_usage),
            }
        }
        let client = Client::new(opensoup_rocket()).expect("Valid rocket instance");
        let mut response = client
            .get(format!("/cook/{}/usage/recipe/{}", cook.id, recipe.id))
            .dispatch();
        if let Some(body) = response.body_string() {
            if let Ok(json_created_recipe_usage) = serde_json::from_str::<JsonValue>(&body) {
                assert_eq!(json_created_recipe_usage, json!(created_recipe_cook_usage));
                assert_eq!(response.status(), Status::Ok);
            } else {
                panic!("Failed to parse json from response body.");
            }
        } else {
            panic!("Failed to get body from response.");
        }
    }

    #[test]
    fn it_gets_all_role_for_a_given_cook_which_has_just_been_created_and_status_is_ok() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let db_connection = establish_db_connection();
        teardown();

        let cook = NewCook {
            email: String::from("cook0@cuisine.com"),
            name: String::from("John Doe"),
            password: String::from("cook0pwd"),
        };

        let role0 = NewRole {
            label: String::from("cook"),
        };

        let role1 = NewRole {
            label: String::from("admin"),
        };

        let roles = vec![role0, role1];

        let mut created_roles: Vec<Role> = Vec::new();

        for role in roles {
            match NewRole::create(&db_connection, role) {
                Ok(new_role) => created_roles.push(new_role),
                Err(_) => println!("fail to insert new role"),
            }
        }

        if let Ok(created_cook) = NewCook::create(&db_connection, cook) {
            for role in &created_roles {
                let insertable_cook_role = NewCookRole {
                    cook_id: created_cook.id,
                    role_id: role.id,
                };
                if let Ok(_) = NewCookRole::create(&db_connection, insertable_cook_role) {
                    continue;
                } else {
                    panic!("failed to create relation between cook and role");
                }
            }
            let mut response = client
                .get(format!("/cook/{}/role", created_cook.id))
                .dispatch();
            if let Some(body) = response.body_string() {
                if let Ok(json_created_roles) = serde_json::from_str::<JsonValue>(&body) {
                    assert_eq!(json_created_roles, json!(created_roles));
                    assert_eq!(response.status(), Status::Ok);
                } else {
                    panic!("Failed to parse json from response body.");
                }
            } else {
                panic!("Failed to get body from response.");
            }
        } else {
            panic!("Failed to insert new cook");
        }
        teardown();
    }
}
