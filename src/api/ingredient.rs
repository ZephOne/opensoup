use rocket::http::Status;
use rocket::response::status;

use crate::api::Json;
use crate::db::models::{Ingredient, NewIngredient};
use crate::db::DbConnection;

#[post("/", format = "json", data = "<ingredient>")]
pub fn create(
    db_connection: DbConnection,
    ingredient: Json<NewIngredient>,
) -> Result<status::Created<String>, String> {
    let new_ingredient = NewIngredient {
        name: String::from(&ingredient.name),
    };
    match NewIngredient::create(&db_connection, new_ingredient) {
        Ok(created_ingredient) => Ok(status::Created(
            format!("/ingredient/{}", created_ingredient.id),
            None,
        )),
        Err(error) => Err(error.to_string()),
    }
}

#[get("/<id>")]
pub fn read(
    db_connection: DbConnection,
    id: i32,
) -> Result<Json<Ingredient>, status::NotFound<String>> {
    match Ingredient::get(&db_connection, id) {
        Ok(ingredient) => Ok(Json(ingredient)),
        Err(error) => Err(status::NotFound(error.to_string())),
    }
}

#[put("/<id>", format = "json", data = "<ingredient>")]
pub fn update(
    db_connection: DbConnection,
    id: i32,
    ingredient: Json<Ingredient>,
) -> Result<Json<Ingredient>, Status> {
    match Ingredient::get(&db_connection, id) {
        Ok(_) => {
            let new_ingredient = Ingredient {
                id: ingredient.id,
                name: String::from(&ingredient.name),
            };

            match Ingredient::update(&db_connection, id, new_ingredient) {
                Ok(updated_ingredient) => Ok(Json(updated_ingredient)),
                Err(_) => Err(Status::InternalServerError),
            }
        }
        Err(_) => Err(Status::Conflict),
    }
}

#[delete("/<id>")]
pub fn delete(db_connection: DbConnection, id: i32) -> Result<(), Status> {
    match Ingredient::get(&db_connection, id) {
        Ok(_) => match Ingredient::delete(&db_connection, &id) {
            Ok(_) => Ok(()),
            Err(error) => {
                println!("error: {}", error.to_string());
                Err(Status::InternalServerError)
            }
        },
        Err(_) => Err(Status::NotFound),
    }
}

#[get("/")]
pub fn all(db_connection: DbConnection) -> Json<Vec<Ingredient>> {
    let ingredients = Ingredient::all(&db_connection);
    Json(ingredients)
}

#[cfg(test)]
mod tests {
    use diesel::prelude::*;
    use rocket::http::{ContentType, Status};
    use rocket::local::Client;
    use rocket_contrib::json::JsonValue;

    use crate::db::establish_db_connection;
    use crate::db::models::{Ingredient, NewIngredient};
    use crate::db::schema::ingredients::dsl::ingredients as all_ingredient;
    use crate::opensoup_rocket;

    pub fn teardown() {
        let db_connection = establish_db_connection();
        diesel::delete(all_ingredient)
            .execute(&db_connection)
            .expect("unable to delete all ingredients");
    }

    #[test]
    fn it_creates_an_ingredient() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let response = client
            .post("/ingredient/")
            .body("{\"name\":\"orange\"}")
            .header(ContentType::JSON)
            .dispatch();
        assert_eq!(response.status().code, 201);
        teardown();
    }

    #[test]
    fn it_gets_an_ingredient_after_it_as_been_created_and_http_status_is_ok() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let db_connection = establish_db_connection();

        let new_ingredient = NewIngredient {
            name: String::from("strawberry"),
        };

        if let Ok(created_ingredient) = NewIngredient::create(&db_connection, new_ingredient) {
            let mut response = client
                .get(format!("/ingredient/{}", created_ingredient.id))
                .dispatch();
            assert_eq!(response.status(), Status::Ok);
            assert_eq!(
                response.body_string(),
                Some(format!(
                    "{{\"id\":{},\"name\":\"{}\"}}",
                    created_ingredient.id, created_ingredient.name
                ))
            );
        } else {
            panic!("failed to create ingredient before getting it!")
        }
        teardown();
    }

    #[test]
    fn it_does_not_get_a_non_existing_ingredient_and_http_status_is_not_found() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let response = client.get("/ingredient/1000").dispatch();
        assert_eq!(response.status(), Status::NotFound);
    }

    #[test]
    fn it_updates_an_ingredient_that_has_been_created_and_status_is_ok() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let db_connection = establish_db_connection();

        let ingredient = NewIngredient {
            name: String::from("potato"),
        };

        match NewIngredient::create(&db_connection, ingredient) {
            Ok(created_ingredient) => {
                let new_ingredient = Ingredient {
                    id: created_ingredient.id,
                    name: String::from("sweet potato"),
                };

                let response = client
                    .put(format!("/ingredient/{}", new_ingredient.id))
                    .body(format!(
                        "{{\"id\":{},\"name\":\"{}\"}}",
                        new_ingredient.id, new_ingredient.name
                    ))
                    .header(ContentType::JSON)
                    .dispatch();
                assert_eq!(response.status(), Status::Ok);
            }
            Err(error) => println!("error: {}", error.to_string()),
        }
        teardown();
    }

    #[test]
    fn it_does_not_update_an_ingredient_if_the_ingredient_does_not_exist_and_status_is_not_found() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");

        let response = client
            .put("/ingredient/1000")
            .body("{\"id\":1000,\"name\":\"potato\"}")
            .header(ContentType::JSON)
            .dispatch();
        assert_eq!(response.status(), Status::Conflict);
    }

    #[test]
    fn it_gets_all_ingredient_and_status_is_ok() {
        teardown();
        let db_connection = establish_db_connection();

        let ingredient0 = NewIngredient {
            name: String::from("lemon"),
        };
        let ingredient1 = NewIngredient {
            name: String::from("raspberry"),
        };

        let ingredients = vec![ingredient1, ingredient0];
        let mut created_ingredients: Vec<Ingredient> = Vec::new();

        for ingredient in ingredients {
            if let Ok(new_ingredient) = NewIngredient::create(&db_connection, ingredient) {
                created_ingredients.push(new_ingredient);
            } else {
                panic!("fail to insert new ingredient");
            }
        }

        created_ingredients.reverse();
        println!("created_ingredients size: {}", created_ingredients.len());

        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let mut response = client.get("/ingredient/").dispatch();
        if let Some(body) = response.body_string() {
            if let Ok(json_created_ingredients) = serde_json::from_str::<JsonValue>(&body) {
                assert_eq!(json_created_ingredients, json!(created_ingredients));
                assert_eq!(response.status(), Status::Ok);
            } else {
                panic!("Failed to parse json from response body.");
            }
        } else {
            panic!("Failed to get body from response.");
        }
        teardown();
    }

    #[test]
    fn it_deletes_an_ingredient_which_has_just_been_created_and_status_is_ok() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let db_connection = establish_db_connection();

        let ingredient = NewIngredient {
            name: String::from("blackberry"),
        };

        if let Ok(created_ingredient) = NewIngredient::create(&db_connection, ingredient) {
            let response = client
                .delete(format!("/ingredient/{}", created_ingredient.id))
                .dispatch();
            assert_eq!(response.status(), Status::Ok);
        }
        teardown();
    }

    #[test]
    fn it_cannot_delete_a_non_existing_ingredient_and_status_is_internal_server_error() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let response = client.delete("/ingredient/1000").dispatch();
        assert_eq!(response.status(), Status::NotFound);
    }
}
