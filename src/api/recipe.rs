use chrono::Utc;
use rocket::http::Status;
use rocket::response::status;

use crate::api::Json;
use crate::db::models::{Ingredient, NewRecipe, Recipe};
use crate::db::DbConnection;

#[post("/", format = "json", data = "<recipe>")]
pub fn create(
    db_connection: DbConnection,
    recipe: Json<NewRecipe>,
) -> Result<status::Created<String>, String> {
    let new_recipe = NewRecipe {
        name: String::from(&recipe.name),
        time_to_cook_label_id: recipe.time_to_cook_label_id,
        content: String::from(&recipe.content),
        cook_id: recipe.cook_id,
    };
    match NewRecipe::create(&db_connection, new_recipe) {
        Ok(created_recipe) => Ok(status::Created(
            format!("/recipe/{}", created_recipe.id),
            None,
        )),
        Err(error) => Err(error.to_string()),
    }
}

#[get("/<id>")]
pub fn read(
    db_connection: DbConnection,
    id: i32,
) -> Result<Json<Recipe>, status::NotFound<String>> {
    match Recipe::get(&db_connection, id) {
        Ok(recipe) => Ok(Json(recipe)),
        Err(error) => Err(status::NotFound(error.to_string())),
    }
}

#[put("/<id>", format = "json", data = "<recipe>")]
pub fn update(
    db_connection: DbConnection,
    id: i32,
    recipe: Json<Recipe>,
) -> Result<Json<Recipe>, Status> {
    match Recipe::get(&db_connection, id) {
        Ok(_) => {
            let new_recipe = Recipe {
                id: recipe.id,
                name: String::from(&recipe.name),
                time_to_cook_label_id: recipe.time_to_cook_label_id,
                content: String::from(&recipe.content),
                creation_date: recipe.creation_date,
                update_date: Some(Utc::now().naive_utc()),
                cook_id: recipe.cook_id,
            };

            match Recipe::update(&db_connection, new_recipe) {
                Ok(updated_recipe) => Ok(Json(updated_recipe)),
                Err(_) => Err(Status::InternalServerError),
            }
        }
        Err(_) => Err(Status::Conflict),
    }
}

#[delete("/<id>")]
pub fn delete(db_connection: DbConnection, id: i32) -> Result<(), Status> {
    match Recipe::get(&db_connection, id) {
        Ok(_) => match Recipe::delete(&db_connection, &id) {
            Ok(_) => Ok(()),
            Err(error) => {
                println!("error: {}", error.to_string());
                Err(Status::InternalServerError)
            }
        },
        Err(_) => Err(Status::NotFound),
    }
}

#[get("/")]
pub fn all(db_connection: DbConnection) -> Json<Vec<Recipe>> {
    let recipes = Recipe::all(&db_connection);
    Json(recipes)
}

#[get("/<id>/ingredient")]
pub fn all_ingredient_by_recipe(db_connection: DbConnection, id: i32) -> Json<Vec<Ingredient>> {
    let ingredients = Ingredient::all_by_recipe(&db_connection, id);
    Json(ingredients)
}

#[cfg(test)]
mod tests {
    use chrono::Utc;
    use diesel::prelude::*;
    use rocket::http::{ContentType, Status};
    use rocket::local::Client;
    use rocket_contrib::json::JsonValue;

    use crate::db::establish_db_connection;
    use crate::db::models::{
        Ingredient, NewCook, NewIngredient, NewRecipe, NewTimeToCookLabel, NewUnit, Recipe,
    };
    use crate::db::schema::cooks::dsl::cooks as all_cook;
    use crate::db::schema::ingredient_recipe_units;
    use crate::db::schema::ingredient_recipe_units::dsl::ingredient_recipe_units as all_ingredient_recipe_unit;
    use crate::db::schema::ingredients::dsl::ingredients as all_ingredient;
    use crate::db::schema::recipes::dsl::recipes as all_recipe;
    use crate::db::schema::time_to_cook_labels::dsl::time_to_cook_labels as all_time_to_cook_label;
    use crate::db::schema::units::dsl::units as all_unit;
    use crate::opensoup_rocket;

    pub fn teardown() {
        let db_connection = establish_db_connection();
        diesel::delete(all_unit)
            .execute(&db_connection)
            .expect("unable to delete all units");
        diesel::delete(all_ingredient_recipe_unit)
            .execute(&db_connection)
            .expect("unable to delete all ingredient_recipe_units");
        diesel::delete(all_recipe)
            .execute(&db_connection)
            .expect("unable to delete all recipes");
        diesel::delete(all_cook)
            .execute(&db_connection)
            .expect("unable to delete all cooks");
        diesel::delete(all_ingredient)
            .execute(&db_connection)
            .expect("unable to delete all ingredients");
        diesel::delete(all_time_to_cook_label)
            .execute(&db_connection)
            .expect("unable to delete all time_to_cook_labels");
    }

    #[test]
    fn it_creates_a_recipe() {
        teardown();
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let db_connection = establish_db_connection();

        let new_time_to_cook_label = NewTimeToCookLabel {
            name: String::from("fast"),
        };
        let new_cook = NewCook {
            email: String::from("peter.pan@neverland.com"),
            name: String::from("Peter Pan"),
            password: String::from("nevergrowup"),
        };
        let cook = NewCook::create(&db_connection, new_cook).expect("Failed to create cook");
        let time_to_cook_label = NewTimeToCookLabel::create(&db_connection, new_time_to_cook_label)
            .expect("Failed to create label");
        let response = client
            .post("/recipe/")
            .body(format!(
                "{{
            		\"name\":\"arancini\",
            		\"time_to_cook_label_id\":{},
            		\"content\":\"https://www.bbcgoodfood.com/recipes/arancini-balls\",
            		\"cook_id\":{}
            	}}",
                time_to_cook_label.id, cook.id
            ))
            .header(ContentType::JSON)
            .dispatch();
        assert_eq!(response.status().code, 201);
    }

    #[test]
    fn it_cannot_create_a_recipe() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let response = client
            .post("/recipe/")
            .body("{\"name\":\"Recipe name\",\"password\":\"Recipe content\"}")
            .header(ContentType::JSON)
            .dispatch();
        assert_eq!(response.status(), Status::UnprocessableEntity);
    }

    #[test]
    fn it_gets_a_recipe_after_it_as_been_created_and_http_status_is_ok() {
        teardown();
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let db_connection = establish_db_connection();

        let cook = NewCook::create(
            &db_connection,
            NewCook {
                email: String::from("peter.pan@neverland.com"),
                name: String::from("Peter Pan"),
                password: String::from("nevergrowup"),
            },
        )
        .expect("Cannot create cook");

        let time_to_cook_label = NewTimeToCookLabel::create(
            &db_connection,
            NewTimeToCookLabel {
                name: String::from("long"),
            },
        )
        .expect("Failed to create time to cook label");

        if let Ok(recipe) = NewRecipe::create(
            &db_connection,
            NewRecipe {
                name: String::from("Recipe name"),
                time_to_cook_label_id: Some(time_to_cook_label.id),
                content: String::from("Recipe content"),
                cook_id: cook.id,
            },
        ) {
            let mut response = client.get(format!("/recipe/{}", recipe.id)).dispatch();
            assert_eq!(response.status(), Status::Ok);
            assert_eq!(
                response.body_string(),
                Some(format!(
                    "{{\"id\":{},\"name\":\"{}\",\"time_to_cook_label_id\":{},\"content\":\"{}\",\"creation_date\":\"{}\",\"update_date\":null,\"cook_id\":{}}}",
                    recipe.id, recipe.name, recipe.time_to_cook_label_id.unwrap(), recipe.content, recipe.creation_date.format("%FT%T%.6f"), recipe.cook_id
                ))
            );
        } else {
            panic!("failed to create recipe before getting it!")
        }
    }

    #[test]
    fn it_does_not_get_a_non_existing_recipe_and_http_status_is_not_found() {
        teardown();
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let response = client.get("/recipe/1000").dispatch();
        assert_eq!(response.status(), Status::NotFound);
    }

    #[test]
    fn it_updates_a_recipe_that_has_been_created_and_status_is_ok() {
        teardown();
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let db_connection = establish_db_connection();

        let cook = NewCook::create(
            &db_connection,
            NewCook {
                email: String::from("peter.pan@neverland.com"),
                name: String::from("Peter Pan"),
                password: String::from("nevergrowup"),
            },
        )
        .expect("Failed to create cook");

        let time_to_cook_label = NewTimeToCookLabel::create(
            &db_connection,
            NewTimeToCookLabel {
                name: String::from("long"),
            },
        )
        .expect("Failed to create time to cook label");

        if let Ok(recipe) = NewRecipe::create(
            &db_connection,
            NewRecipe {
                name: String::from("Recipe name"),
                time_to_cook_label_id: Some(time_to_cook_label.id),
                content: String::from("Recipe content"),
                cook_id: cook.id,
            },
        ) {
            let new_recipe = Recipe {
                id: recipe.id,
                name: String::from("Recipe name"),
                time_to_cook_label_id: Some(time_to_cook_label.id),
                content: String::from("New recipe content"),
                creation_date: recipe.creation_date,
                update_date: None,
                cook_id: cook.id,
            };

            let response = client
                .put(format!("/recipe/{}", new_recipe.id))
                .body(format!(
                    "{{\"id\":{},\"name\":\"{}\",\"time_to_cook_label_id\":{},\"content\":\"{}\",\"creation_date\":\"{}\",\"update_date\":\"{}\",\"cook_id\":{}}}",
                    recipe.id, recipe.name, recipe.time_to_cook_label_id.unwrap(), recipe.content, recipe.creation_date.format("%FT%T%.6f"), Utc::now().naive_utc().format("%FT%T%.6f"), recipe.cook_id
                ))
                .header(ContentType::JSON)
                .dispatch();
            assert_eq!(response.status(), Status::Ok);
        } else {
            panic!("Failed to insert new recipe before updating it.");
        }
    }

    #[test]
    fn it_does_not_update_a_recipe_if_the_recipe_does_not_exist_and_status_is_not_found() {
        teardown();
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");

        let response = client
            .put("/recipe/1000")
            .body(
                "{\"id\":1000,\"name\":\"Recipe name\",\"time_to_cook_label_id\":1,\"content\":\"Recipe Content\",\"creation_date\":\"2020-12-11T16:10:13.940512\",\"update_date\":null,\"cook_id\":1}"
            )
            .header(ContentType::JSON)
            .dispatch();
        assert_eq!(response.status(), Status::Conflict);
    }

    #[test]
    fn it_gets_all_recipe_and_status_is_ok() {
        teardown();
        let db_connection = establish_db_connection();

        let cook0 = NewCook::create(
            &db_connection,
            NewCook {
                email: String::from("cook0@cuisine.com"),
                name: String::from("John Doe"),
                password: String::from("cook0pwd"),
            },
        )
        .expect("Failed to create cook");
        let cook1 = NewCook::create(
            &db_connection,
            NewCook {
                email: String::from("cook1@cuisine.com"),
                name: String::from("Jack Slave"),
                password: String::from("cook1pwd"),
            },
        )
        .expect("Failed to create cook");

        let cook2 = NewCook::create(
            &db_connection,
            NewCook {
                email: String::from("cook2@cuisine.com"),
                name: String::from("Henry the pilot"),
                password: String::from("cook2pwd"),
            },
        )
        .expect("Failed to create cook");

        let time_to_cook_label0 = NewTimeToCookLabel::create(
            &db_connection,
            NewTimeToCookLabel {
                name: String::from("fast"),
            },
        )
        .expect("Failed to create time to cook label");

        let time_to_cook_label1 = NewTimeToCookLabel::create(
            &db_connection,
            NewTimeToCookLabel {
                name: String::from("long"),
            },
        )
        .expect("Failed to create time to cook label");

        let recipe0 = NewRecipe::create(
            &db_connection,
            NewRecipe {
                name: String::from("Recipe name 0"),
                time_to_cook_label_id: Some(time_to_cook_label0.id),
                content: String::from("Recipe content 0"),
                cook_id: cook0.id,
            },
        )
        .expect("Failed to insert new recipe");

        let recipe1 = NewRecipe::create(
            &db_connection,
            NewRecipe {
                name: String::from("Recipe name 1"),
                time_to_cook_label_id: Some(time_to_cook_label0.id),
                content: String::from("Recipe content 1"),
                cook_id: cook1.id,
            },
        )
        .expect("Failed to insert new recipe");

        let recipe2 = NewRecipe::create(
            &db_connection,
            NewRecipe {
                name: String::from("Recipe name 2"),
                time_to_cook_label_id: Some(time_to_cook_label0.id),
                content: String::from("Recipe content 2"),
                cook_id: cook2.id,
            },
        )
        .expect("Failed to insert new recipe");

        let recipe3 = NewRecipe::create(
            &db_connection,
            NewRecipe {
                name: String::from("Recipe name 3"),
                time_to_cook_label_id: Some(time_to_cook_label1.id),
                content: String::from("Recipe content 3"),
                cook_id: cook0.id,
            },
        )
        .expect("Failed to insert new recipe");

        let recipe4 = NewRecipe::create(
            &db_connection,
            NewRecipe {
                name: String::from("Recipe name 4"),
                time_to_cook_label_id: Some(time_to_cook_label1.id),
                content: String::from("Recipe content 4"),
                cook_id: cook1.id,
            },
        )
        .expect("Failed to insert new recipe");

        let recipe5 = NewRecipe::create(
            &db_connection,
            NewRecipe {
                name: String::from("Recipe name 5"),
                time_to_cook_label_id: Some(time_to_cook_label1.id),
                content: String::from("Recipe content 5"),
                cook_id: cook2.id,
            },
        )
        .expect("Failed to insert new recipe");

        let recipes = vec![recipe5, recipe4, recipe3, recipe2, recipe1, recipe0];

        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let mut response = client.get("/recipe/").dispatch();
        if let Some(body) = response.body_string() {
            if let Ok(json_recipes) = serde_json::from_str::<JsonValue>(&body) {
                assert_eq!(json_recipes, json!(recipes));
                assert_eq!(response.status(), Status::Ok);
            } else {
                panic!("Failed to parse json from response body.");
            }
        } else {
            panic!("Failed to get body from response.");
        }
    }

    #[test]
    fn it_deletes_a_recipe_which_has_just_been_created_and_status_is_ok() {
        teardown();
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let db_connection = establish_db_connection();

        let cook = NewCook::create(
            &db_connection,
            NewCook {
                email: String::from("peter.pan@neverland.com"),
                name: String::from("Peter Pan"),
                password: String::from("nevergrowup"),
            },
        )
        .expect("Failed to create cook");

        let time_to_cook_label = NewTimeToCookLabel::create(
            &db_connection,
            NewTimeToCookLabel {
                name: String::from("fast"),
            },
        )
        .expect("Failed to create time to cook label");

        if let Ok(created_recipe) = NewRecipe::create(
            &db_connection,
            NewRecipe {
                name: String::from("Recipe name"),
                time_to_cook_label_id: Some(time_to_cook_label.id),
                content: String::from("Recipe content"),
                cook_id: cook.id,
            },
        ) {
            let response = client
                .delete(format!("/recipe/{}", created_recipe.id))
                .dispatch();
            assert_eq!(response.status(), Status::Ok);
        }
    }

    #[test]
    fn it_cannot_delete_a_non_existing_user_and_status_is_internal_server_error() {
        teardown();
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let response = client.delete("/recipe/1000").dispatch();
        assert_eq!(response.status(), Status::NotFound);
    }

    #[test]
    fn it_gets_all_ingredients_for_a_given_recipe_and_status_is_ok() {
        teardown();
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let db_connection = establish_db_connection();

        let cook = NewCook::create(
            &db_connection,
            NewCook {
                email: String::from("peter.pan@neverland.com"),
                name: String::from("Peter Pan"),
                password: String::from("nevergrowup"),
            },
        )
        .expect("Failed to create cook");

        let time_to_cook_label = NewTimeToCookLabel::create(
            &db_connection,
            NewTimeToCookLabel {
                name: String::from("fast"),
            },
        )
        .expect("Failed to create time to cook label");

        let recipe_content = String::from(
          "Start by making the dipping sauce by adding the dipping sauce ingredients (except the chopped peanuts) to a food processor and blending until smooth.
          Transfer to the container(s) you want to serve it in, sprinkle on the chopped peanuts, and put it in the fridge until needed.

          Cut pork into bite-size strips, and remove any fat/gristle.
          Add the sugar in an even layer to the bottom of a heavy skillet on medium-low heat.
          It may take a few minutes for anything to happen... but if nothing happens for like 10 minutes, increase the heat a bit.
          Watch the sugar carefully.
          Once it melts and turns a nice caramel color, add the pork and stir until it's coated.
          It's ok if it appears to be a sticky mess.

          Add in the fish sauce and salt & pepper and stir.
          Cover the pan and cook for about two minutes.
          Add in the garlic and sesame oil and stir.
          Cook uncovered for about 20 minutes, stirring occasionally, until the sauce reduces and the pork is caramelized in the delicious sticky sweet sauce.
          When it's cooked, transfer to a bowl or plate.

          While the pork is caramelizing, it's a good idea to start chopping the vegetables and cook the vermicelli to make use of the time.
          Peel the carrots and cut them into matchsticks.
          I don't peel the cucumber; I just cut it into matchsticks.
          Chop the green parts of the scallions.
          Tear the lettuce into bite-size pieces.
          Set the vegetables aside on a plate so they're ready to go.
          The vermicelli only takes a few minutes to cook.
          Add it to boiling water and follow package directions.
          I drain it using a sieve and rinse it under cool water so it cools faster.
          Place in a bowl and set aside.

          When you've completed the above steps, it'll finally be time to assemble the summer rolls 🙂 Fill a large bowl full of cool water.
          Wet a piece of rice paper and then place it on the surface you'll be assembling the rolls on.

          Add the vermicelli, vegetables, and pork to the top third of the rice paper.
          Fold the sides over slightly and then fold the top over so it just touches the filling.
          Tuck in the filling with your hands as you roll it towards you.
          Keep in mind you want the rolls to be fairly firm, but if you roll them too tightly, you'll break the paper.
          Don't worry... you'll get the hang of it 🙂

          Repeat the process for as many rolls as you have stuffing for.
          Serve the summer rolls with the peanut dipping sauce.
          You can keep the rolls intact or cut them in half.
          Enjoy the fruits of your labor.
          "
        );

        let recipe = NewRecipe::create(
            &db_connection,
            NewRecipe {
                name: String::from("Caramelized Pork Summer Rolls"),
                time_to_cook_label_id: Some(time_to_cook_label.id),
                content: recipe_content,
                cook_id: cook.id,
            },
        )
        .expect("Failed to create recipe");

        let unit_cup = NewUnit::create(
            &db_connection,
            NewUnit {
                name: String::from("cup"),
                symbol: None,
            },
        )
        .expect("Failed to create unit");

        let unit_table_spoon = NewUnit::create(
            &db_connection,
            NewUnit {
                name: String::from("table spoon"),
                symbol: None,
            },
        )
        .expect("Failed to create unit");

        let unit_tea_spoon = NewUnit::create(
            &db_connection,
            NewUnit {
                name: String::from("tea spoon"),
                symbol: None,
            },
        )
        .expect("Failed to create unit");

        let unit_pound = NewUnit::create(
            &db_connection,
            NewUnit {
                name: String::from("pound"),
                symbol: Some(String::from("lb")),
            },
        )
        .expect("Failed to create unit");

        let unit_ounce = NewUnit::create(
            &db_connection,
            NewUnit {
                name: String::from("ounce"),
                symbol: Some(String::from("oz")),
            },
        )
        .expect("Failed to create unit");

        let mut ingredients: Vec<&Ingredient> = vec![];

        let peanut_butter = NewIngredient::create(
            &db_connection,
            NewIngredient {
                name: String::from("peanut_butter"),
            },
        )
        .expect("Failed to create ingredient");

        ingredients.push(&peanut_butter);

        let lime = NewIngredient::create(
            &db_connection,
            NewIngredient {
                name: String::from("lime"),
            },
        )
        .expect("Failed to create ingredient");

        ingredients.push(&lime);

        let hoisin_sauce = NewIngredient::create(
            &db_connection,
            NewIngredient {
                name: String::from("hoisin sauce"),
            },
        )
        .expect("Failed to create ingredient");

        ingredients.push(&hoisin_sauce);

        let clove_garlic = NewIngredient::create(
            &db_connection,
            NewIngredient {
                name: String::from("clove garlic"),
            },
        )
        .expect("Failed to create ingredient");

        ingredients.push(&clove_garlic);

        let peanut_oil = NewIngredient::create(
            &db_connection,
            NewIngredient {
                name: String::from("clove"),
            },
        )
        .expect("Failed to create ingredient");

        ingredients.push(&peanut_oil);

        let peanuts = NewIngredient::create(
            &db_connection,
            NewIngredient {
                name: String::from("peanuts"),
            },
        )
        .expect("Failed to create ingredient");

        ingredients.push(&peanuts);

        let salt = NewIngredient::create(
            &db_connection,
            NewIngredient {
                name: String::from("salt"),
            },
        )
        .expect("Failed to create ingredient");

        ingredients.push(&salt);

        let pepper = NewIngredient::create(
            &db_connection,
            NewIngredient {
                name: String::from("pepper"),
            },
        )
        .expect("Failed to create ingredient");

        ingredients.push(&pepper);

        let pork_tenderloin = NewIngredient::create(
            &db_connection,
            NewIngredient {
                name: String::from("pork tenderloin"),
            },
        )
        .expect("Failed to create ingredient");

        ingredients.push(&pork_tenderloin);

        let granulated_sugar = NewIngredient::create(
            &db_connection,
            NewIngredient {
                name: String::from("granulated sugar"),
            },
        )
        .expect("Failed to create ingredient");

        ingredients.push(&granulated_sugar);

        let fish_sauce = NewIngredient::create(
            &db_connection,
            NewIngredient {
                name: String::from("fish sauce"),
            },
        )
        .expect("Failed to create ingredient");

        ingredients.push(&fish_sauce);

        let sesame_oil = NewIngredient::create(
            &db_connection,
            NewIngredient {
                name: String::from("sesame oil"),
            },
        )
        .expect("Failed to create ingredient");

        ingredients.push(&sesame_oil);

        let rice_paper = NewIngredient::create(
            &db_connection,
            NewIngredient {
                name: String::from("rice paper"),
            },
        )
        .expect("Failed to create ingredient");

        ingredients.push(&rice_paper);

        let carrot = NewIngredient::create(
            &db_connection,
            NewIngredient {
                name: String::from("carrot"),
            },
        )
        .expect("Failed to create ingredient");

        ingredients.push(&carrot);

        let cucumber = NewIngredient::create(
            &db_connection,
            NewIngredient {
                name: String::from("cucumber"),
            },
        )
        .expect("Failed to create ingredient");

        ingredients.push(&cucumber);

        let scallion = NewIngredient::create(
            &db_connection,
            NewIngredient {
                name: String::from("scallion"),
            },
        )
        .expect("Failed to create ingredient");

        ingredients.push(&scallion);

        let butter_leaf_lettuce = NewIngredient::create(
            &db_connection,
            NewIngredient {
                name: String::from("butter leaf lettuce"),
            },
        )
        .expect("Failed to create ingredient");

        ingredients.push(&butter_leaf_lettuce);

        let vermicelli = NewIngredient::create(
            &db_connection,
            NewIngredient {
                name: String::from("vermicelli"),
            },
        )
        .expect("Failed to create ingredient");

        ingredients.push(&vermicelli);

        diesel::insert_into(all_ingredient_recipe_unit)
            .values((
                ingredient_recipe_units::portions.eq(4),
                ingredient_recipe_units::ingredient_id.eq(&peanut_butter.id),
                ingredient_recipe_units::recipe_id.eq(&recipe.id),
            ))
            .execute(&db_connection)
            .expect("Error creating relation between ingredient, recipe and unit");

        diesel::insert_into(all_ingredient_recipe_unit)
            .values((
                ingredient_recipe_units::portions.eq(4),
                ingredient_recipe_units::quantity.eq(Some(String::from("1"))),
                ingredient_recipe_units::ingredient_id.eq(&lime.id),
                ingredient_recipe_units::recipe_id.eq(&recipe.id),
            ))
            .execute(&db_connection)
            .expect("Error creating relation between ingredient, recipe and unit");

        diesel::insert_into(all_ingredient_recipe_unit)
            .values((
                ingredient_recipe_units::portions.eq(4),
                ingredient_recipe_units::quantity.eq(Some(String::from("3"))),
                ingredient_recipe_units::ingredient_id.eq(&hoisin_sauce.id),
                ingredient_recipe_units::recipe_id.eq(&recipe.id),
                ingredient_recipe_units::unit_id.eq(Some(&unit_table_spoon.id)),
            ))
            .execute(&db_connection)
            .expect("Error creating relation between ingredient, recipe and unit");

        diesel::insert_into(all_ingredient_recipe_unit)
            .values((
                ingredient_recipe_units::portions.eq(4),
                ingredient_recipe_units::quantity.eq(Some(String::from("1"))),
                ingredient_recipe_units::ingredient_id.eq(&clove_garlic.id),
                ingredient_recipe_units::recipe_id.eq(&recipe.id),
            ))
            .execute(&db_connection)
            .expect("Error creating relation between ingredient, recipe and unit");

        diesel::insert_into(all_ingredient_recipe_unit)
            .values((
                ingredient_recipe_units::portions.eq(4),
                ingredient_recipe_units::quantity.eq(Some(String::from("2"))),
                ingredient_recipe_units::ingredient_id.eq(&peanut_oil.id),
                ingredient_recipe_units::recipe_id.eq(&recipe.id),
                ingredient_recipe_units::unit_id.eq(Some(&unit_table_spoon.id)),
            ))
            .execute(&db_connection)
            .expect("Error creating relation between ingredient, recipe and unit");

        diesel::insert_into(all_ingredient_recipe_unit)
            .values((
                ingredient_recipe_units::portions.eq(4),
                ingredient_recipe_units::ingredient_id.eq(&peanuts.id),
                ingredient_recipe_units::recipe_id.eq(&recipe.id),
            ))
            .execute(&db_connection)
            .expect("Error creating relation between ingredient, recipe and unit");

        diesel::insert_into(all_ingredient_recipe_unit)
            .values((
                ingredient_recipe_units::portions.eq(4),
                ingredient_recipe_units::ingredient_id.eq(&salt.id),
                ingredient_recipe_units::recipe_id.eq(&recipe.id),
            ))
            .execute(&db_connection)
            .expect("Error creating relation between ingredient, recipe and unit");

        diesel::insert_into(all_ingredient_recipe_unit)
            .values((
                ingredient_recipe_units::portions.eq(4),
                ingredient_recipe_units::ingredient_id.eq(&pepper.id),
                ingredient_recipe_units::recipe_id.eq(&recipe.id),
            ))
            .execute(&db_connection)
            .expect("Error creating relation between ingredient, recipe and unit");

        diesel::insert_into(all_ingredient_recipe_unit)
            .values((
                ingredient_recipe_units::portions.eq(4),
                ingredient_recipe_units::quantity.eq(Some(String::from("1"))),
                ingredient_recipe_units::ingredient_id.eq(&pork_tenderloin.id),
                ingredient_recipe_units::recipe_id.eq(&recipe.id),
                ingredient_recipe_units::unit_id.eq(Some(&unit_pound.id)),
            ))
            .execute(&db_connection)
            .expect("Error creating relation between ingredient, recipe and unit");

        diesel::insert_into(all_ingredient_recipe_unit)
            .values((
                ingredient_recipe_units::portions.eq(4),
                ingredient_recipe_units::quantity.eq(Some(String::from("3/4"))),
                ingredient_recipe_units::ingredient_id.eq(&granulated_sugar.id),
                ingredient_recipe_units::recipe_id.eq(&recipe.id),
                ingredient_recipe_units::unit_id.eq(Some(&unit_cup.id)),
            ))
            .execute(&db_connection)
            .expect("Error creating relation between ingredient, recipe and unit");

        diesel::insert_into(all_ingredient_recipe_unit)
            .values((
                ingredient_recipe_units::portions.eq(4),
                ingredient_recipe_units::quantity.eq(Some(String::from("2"))),
                ingredient_recipe_units::ingredient_id.eq(&fish_sauce.id),
                ingredient_recipe_units::recipe_id.eq(&recipe.id),
                ingredient_recipe_units::unit_id.eq(Some(&unit_table_spoon.id)),
            ))
            .execute(&db_connection)
            .expect("Error creating relation between ingredient, recipe and unit");

        diesel::insert_into(all_ingredient_recipe_unit)
            .values((
                ingredient_recipe_units::portions.eq(4),
                ingredient_recipe_units::quantity.eq(Some(String::from("1"))),
                ingredient_recipe_units::ingredient_id.eq(&sesame_oil.id),
                ingredient_recipe_units::recipe_id.eq(&recipe.id),
                ingredient_recipe_units::unit_id.eq(Some(&unit_tea_spoon.id)),
            ))
            .execute(&db_connection)
            .expect("Error creating relation between ingredient, recipe and unit");

        diesel::insert_into(all_ingredient_recipe_unit)
            .values((
                ingredient_recipe_units::portions.eq(4),
                ingredient_recipe_units::quantity.eq(Some(String::from("8"))),
                ingredient_recipe_units::ingredient_id.eq(&rice_paper.id),
                ingredient_recipe_units::recipe_id.eq(&recipe.id),
            ))
            .execute(&db_connection)
            .expect("Error creating relation between ingredient, recipe and unit");

        diesel::insert_into(all_ingredient_recipe_unit)
            .values((
                ingredient_recipe_units::portions.eq(4),
                ingredient_recipe_units::quantity.eq(Some(String::from("2"))),
                ingredient_recipe_units::ingredient_id.eq(&carrot.id),
                ingredient_recipe_units::recipe_id.eq(&recipe.id),
            ))
            .execute(&db_connection)
            .expect("Error creating relation between ingredient, recipe and unit");

        diesel::insert_into(all_ingredient_recipe_unit)
            .values((
                ingredient_recipe_units::portions.eq(4),
                ingredient_recipe_units::quantity.eq(Some(String::from("1/2"))),
                ingredient_recipe_units::ingredient_id.eq(&cucumber.id),
                ingredient_recipe_units::recipe_id.eq(&recipe.id),
            ))
            .execute(&db_connection)
            .expect("Error creating relation between ingredient, recipe and unit");

        diesel::insert_into(all_ingredient_recipe_unit)
            .values((
                ingredient_recipe_units::portions.eq(4),
                ingredient_recipe_units::quantity.eq(Some(String::from("4"))),
                ingredient_recipe_units::ingredient_id.eq(&scallion.id),
                ingredient_recipe_units::recipe_id.eq(&recipe.id),
            ))
            .execute(&db_connection)
            .expect("Error creating relation between ingredient, recipe and unit");

        diesel::insert_into(all_ingredient_recipe_unit)
            .values((
                ingredient_recipe_units::portions.eq(4),
                ingredient_recipe_units::quantity.eq(Some(String::from("1/2"))),
                ingredient_recipe_units::ingredient_id.eq(&butter_leaf_lettuce.id),
                ingredient_recipe_units::recipe_id.eq(&recipe.id),
            ))
            .execute(&db_connection)
            .expect("Error creating relation between ingredient, recipe and unit");

        diesel::insert_into(all_ingredient_recipe_unit)
            .values((
                ingredient_recipe_units::portions.eq(4),
                ingredient_recipe_units::quantity.eq(Some(String::from("4.4"))),
                ingredient_recipe_units::ingredient_id.eq(&vermicelli.id),
                ingredient_recipe_units::recipe_id.eq(&recipe.id),
                ingredient_recipe_units::unit_id.eq(Some(&unit_ounce.id)),
            ))
            .execute(&db_connection)
            .expect("Error creating relation between ingredient, recipe and unit");

        let mut response = client
            .get(format!("/recipe/{}/ingredient", &recipe.id))
            .dispatch();
        if let Some(body) = response.body_string() {
            if let Ok(json_ingredients) = serde_json::from_str::<JsonValue>(&body) {
                assert_eq!(json_ingredients, json!(ingredients));
                assert_eq!(response.status(), Status::Ok);
            } else {
                panic!("Failed to parse json from response body.");
            }
        } else {
            panic!("Failed to get body from response.");
        }
    }
}
