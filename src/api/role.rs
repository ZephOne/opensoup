use rocket::http::Status;
use rocket::response::status;

use crate::api::Json;
use crate::db::models::{Cook, CookRole, NewRole, Role};
use crate::db::DbConnection;

#[post("/", format = "json", data = "<role>")]
pub fn create(
    db_connection: DbConnection,
    role: Json<NewRole>,
) -> Result<status::Created<String>, String> {
    let new_role = NewRole {
        label: String::from(&role.label),
    };
    match NewRole::create(&db_connection, new_role) {
        Ok(created_role) => Ok(status::Created(format!("/role/{}", created_role.id), None)),
        Err(error) => Err(error.to_string()),
    }
}

#[get("/<id>")]
pub fn read(db_connection: DbConnection, id: i32) -> Result<Json<Role>, status::NotFound<String>> {
    match Role::get(&db_connection, id) {
        Ok(role) => Ok(Json(role)),
        Err(error) => Err(status::NotFound(error.to_string())),
    }
}

#[put("/<id>", format = "json", data = "<role>")]
pub fn update(
    db_connection: DbConnection,
    id: i32,
    role: Json<Role>,
) -> Result<Json<Role>, Status> {
    match Role::get(&db_connection, id) {
        Ok(_) => {
            let new_role = Role {
                id: role.id,
                label: String::from(&role.label),
            };

            match Role::update(&db_connection, id, new_role) {
                Ok(updated_role) => Ok(Json(updated_role)),
                Err(_) => Err(Status::InternalServerError),
            }
        }
        Err(_) => Err(Status::Conflict),
    }
}

#[delete("/<id>")]
pub fn delete(db_connection: DbConnection, id: i32) -> Result<(), Status> {
    match Role::get(&db_connection, id) {
        Ok(_) => match Role::delete(&db_connection, &id) {
            Ok(_) => Ok(()),
            Err(error) => {
                println!("error: {}", error.to_string());
                Err(Status::InternalServerError)
            }
        },
        Err(_) => Err(Status::NotFound),
    }
}

#[get("/")]
pub fn all(db_connection: DbConnection) -> Json<Vec<Role>> {
    let roles = Role::all(&db_connection);
    Json(roles)
}

#[get("/<id>/cook")]
pub fn all_cook_by_role(
    db_connection: DbConnection,
    id: i32,
) -> Result<Json<Vec<Cook>>, status::NotFound<String>> {
    match CookRole::all_cook_by_role(&db_connection, id) {
        Ok(cooks) => Ok(Json(cooks)),
        Err(error) => Err(status::NotFound(error.to_string())),
    }
}

#[cfg(test)]
mod tests {
    use diesel::prelude::*;
    use rocket::http::{ContentType, Status};
    use rocket::local::Client;
    use rocket_contrib::json::JsonValue;

    use crate::db::establish_db_connection;
    use crate::db::models::{Cook, NewCook, NewCookRole, NewRole, Role};
    use crate::db::schema::cooks::dsl::cooks as all_cook;
    use crate::db::schema::roles::dsl::roles as all_role;
    use crate::opensoup_rocket;

    pub fn teardown() {
        let db_connection = establish_db_connection();
        diesel::delete(all_role)
            .execute(&db_connection)
            .expect("unable to delete all roles");
        diesel::delete(all_cook)
            .execute(&db_connection)
            .expect("unable to delete all cooks");
    }

    #[test]
    fn it_creates_a_role() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let response = client
            .post("/role/")
            .body("{\"label\":\"role\"}")
            .header(ContentType::JSON)
            .dispatch();
        assert_eq!(response.status().code, 201);
        teardown();
    }

    #[test]
    fn it_gets_a_role_after_it_as_been_created_and_http_status_is_ok() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let db_connection = establish_db_connection();

        let new_role = NewRole {
            label: String::from("administrator"),
        };

        if let Ok(created_role) = NewRole::create(&db_connection, new_role) {
            let mut response = client.get(format!("/role/{}", created_role.id)).dispatch();
            assert_eq!(response.status(), Status::Ok);
            assert_eq!(
                response.body_string(),
                Some(format!(
                    "{{\"id\":{},\"label\":\"{}\"}}",
                    created_role.id, created_role.label
                ))
            );
        } else {
            panic!("failed to create role before getting it!")
        }
        teardown();
    }

    #[test]
    fn it_does_not_get_a_non_existing_role_and_http_status_is_not_found() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let response = client.get("/role/1000").dispatch();
        assert_eq!(response.status(), Status::NotFound);
    }

    #[test]
    fn it_updates_a_role_that_has_been_created_and_status_is_ok() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let db_connection = establish_db_connection();

        let role = NewRole {
            label: String::from("cook"),
        };

        match NewRole::create(&db_connection, role) {
            Ok(created_role) => {
                let new_role = Role {
                    id: created_role.id,
                    label: String::from("administrator"),
                };

                let response = client
                    .put(format!("/role/{}", new_role.id))
                    .body(format!(
                        "{{\"id\":{},\"label\":\"{}\"}}",
                        new_role.id, new_role.label
                    ))
                    .header(ContentType::JSON)
                    .dispatch();
                assert_eq!(response.status(), Status::Ok);
            }
            Err(error) => println!("error: {}", error.to_string()),
        }
        teardown();
    }

    #[test]
    fn it_does_not_update_a_role_if_the_role_does_not_exist_and_status_is_not_found() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");

        let response = client
            .put("/role/1000")
            .body("{\"id\":1000,\"label\":\"cook\"}")
            .header(ContentType::JSON)
            .dispatch();
        assert_eq!(response.status(), Status::Conflict);
    }

    #[test]
    fn it_gets_all_role_and_status_is_ok() {
        teardown();
        let db_connection = establish_db_connection();

        let role0 = NewRole {
            label: String::from("administrator"),
        };
        let role1 = NewRole {
            label: String::from("cook"),
        };

        let roles = vec![role0, role1];
        let mut created_roles: Vec<Role> = Vec::new();

        for role in roles {
            if let Ok(new_role) = NewRole::create(&db_connection, role) {
                created_roles.push(new_role);
            } else {
                panic!("fail to insert new role");
            }
        }

        created_roles.reverse();
        println!("created_roles size: {}", created_roles.len());

        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let mut response = client.get("/role/").dispatch();
        if let Some(body) = response.body_string() {
            if let Ok(json_created_roles) = serde_json::from_str::<JsonValue>(&body) {
                assert_eq!(json_created_roles, json!(created_roles));
                assert_eq!(response.status(), Status::Ok);
            } else {
                panic!("Failed to parse json from response body.");
            }
        } else {
            panic!("Failed to get body from response.");
        }
        teardown();
    }

    #[test]
    fn it_deletes_a_role_which_has_just_been_created_and_status_is_ok() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let db_connection = establish_db_connection();

        let role = NewRole {
            label: String::from("cook"),
        };

        if let Ok(created_role) = NewRole::create(&db_connection, role) {
            let response = client
                .delete(format!("/role/{}", created_role.id))
                .dispatch();
            assert_eq!(response.status(), Status::Ok);
        }
        teardown();
    }

    #[test]
    fn it_cannot_delete_a_non_existing_role_and_status_is_internal_server_error() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let response = client.delete("/role/1000").dispatch();
        assert_eq!(response.status(), Status::NotFound);
    }

    #[test]
    fn it_gets_all_cook_for_a_given_role_which_has_just_been_created_and_status_is_ok() {
        let client = Client::new(opensoup_rocket()).expect("valid rocket instance");
        let db_connection = establish_db_connection();

        let role = NewRole {
            label: String::from("cook"),
        };

        let cook0 = NewCook {
            email: String::from("cook0@cuisine.com"),
            name: String::from("John Doe"),
            password: String::from("cook0pwd"),
        };
        let cook1 = NewCook {
            email: String::from("cook1@cuisine.com"),
            name: String::from("Jack Slave"),
            password: String::from("cook1pwd"),
        };
        let cook2 = NewCook {
            email: String::from("cook2@cuisine.com"),
            name: String::from("Henry the pilot"),
            password: String::from("cook2pwd"),
        };

        let cooks = vec![cook0, cook1, cook2];

        let mut created_cooks: Vec<Cook> = Vec::new();

        for cook in cooks {
            match NewCook::create(&db_connection, cook) {
                Ok(new_cook) => created_cooks.push(new_cook),
                Err(_) => println!("fail to insert new cook"),
            }
        }

        if let Ok(created_role) = NewRole::create(&db_connection, role) {
            for cook in &created_cooks {
                let insertable_cook_role = NewCookRole {
                    cook_id: cook.id,
                    role_id: created_role.id,
                };
                if let Ok(_) = NewCookRole::create(&db_connection, insertable_cook_role) {
                    continue;
                } else {
                    panic!("failed to create relation between cook and role");
                }
            }
            let mut response = client
                .get(format!("/role/{}/cook", created_role.id))
                .dispatch();
            if let Some(body) = response.body_string() {
                if let Ok(json_created_cooks) = serde_json::from_str::<JsonValue>(&body) {
                    assert_eq!(json_created_cooks, json!(created_cooks));
                    assert_eq!(response.status(), Status::Ok);
                } else {
                    panic!("Failed to parse json from response body.");
                }
            } else {
                panic!("Failed to get body from response.");
            }
        } else {
            panic!("Failed to insert new role");
        }
        teardown();
    }
}
