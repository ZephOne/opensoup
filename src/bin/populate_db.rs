extern crate opensoup;

use std::fs::File;
use std::io;
use std::io::prelude::*;

use chrono::NaiveDate;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use toml::Value;

use opensoup::db::models::{
    Cook, Ingredient, NewCook, NewIngredient, NewRecipe, NewRole, NewUnit, Recipe, Role,
    TimeToCookLabel, Unit,
};
use opensoup::db::schema::cook_roles;
use opensoup::db::schema::cook_roles::dsl::cook_roles as all_cook_role;
use opensoup::db::schema::cooks::dsl::cooks as all_cook;
use opensoup::db::schema::ingredient_recipe_units;
use opensoup::db::schema::ingredient_recipe_units::dsl::ingredient_recipe_units as all_ingredient_recipe_unit;
use opensoup::db::schema::recipe_cooks;
use opensoup::db::schema::recipe_cooks::dsl::recipe_cooks as all_recipe_cook;
use opensoup::db::schema::time_to_cook_labels;
use opensoup::db::schema::time_to_cook_labels::dsl::time_to_cook_labels as all_time_to_cook_label;

pub fn get_config_file_content() -> Result<String, io::Error> {
    let mut config_file = File::open("Rocket.toml")?;
    let mut config_file_content = String::new();
    config_file.read_to_string(&mut config_file_content)?;
    Ok(config_file_content)
}

pub fn establish_db_connection() -> PgConnection {
    let config_file_content = get_config_file_content().expect("Can't read Rocket.toml");
    let config = config_file_content
        .parse::<Value>()
        .expect("Your Rocket.toml file is not correct");
    let database_url = &config["global"]["databases"]["db"]["url"].as_str().unwrap();
    dbg!(database_url);
    PgConnection::establish(database_url).expect(&format!("Error connecting to {}", database_url))
}

pub fn populate_cooks(db_connection: &PgConnection) {
    let cook0 = NewCook {
        email: String::from("cook0@cuisine.com"),
        name: String::from("John Doe"),
        password: String::from("cook0pwd"),
    };
    let cook1 = NewCook {
        email: String::from("cook1@cuisine.com"),
        name: String::from("Jack Slave"),
        password: String::from("cook1pwd"),
    };
    let cook2 = NewCook {
        email: String::from("cook2@cuisine.com"),
        name: String::from("Henry the pilot"),
        password: String::from("cook2pwd"),
    };
    let cook3 = NewCook {
        email: String::from("cook3@cuisine.com"),
        name: String::from("Sanji Vinsmoke"),
        password: String::from("cook3pwd"),
    };
    let cook4 = NewCook {
        email: String::from("cook4@cuisine.com"),
        name: String::from("King Arthur"),
        password: String::from("cook4pwd"),
    };
    let cook5 = NewCook {
        email: String::from("cook5@cuisine.com"),
        name: String::from("Footix98"),
        password: String::from("cook5pwd"),
    };
    let cook6 = NewCook {
        email: String::from("cook6@cuisine.com"),
        name: String::from("TomTim"),
        password: String::from("cook6pwd"),
    };

    let cooks = vec![cook0, cook1, cook2, cook3, cook4, cook5, cook6];

    for cook in cooks {
        if let Ok(new_cook) = NewCook::create(db_connection, cook) {
            println!("The new cook {} has been created", new_cook.name);
        }
    }

    println!("Table cook has been populated.");
}

pub fn populate_roles(db_connection: &PgConnection) {
    let role0 = NewRole {
        label: String::from("administrator"),
    };
    let role1 = NewRole {
        label: String::from("cook"),
    };

    let roles = vec![role0, role1];

    for role in roles {
        if let Ok(new_role) = NewRole::create(db_connection, role) {
            println!("The new role {} has been created", new_role.label);
        }
    }

    println!("Table role has been populated.");
}

pub fn populate_cook_roles(db_connection: &PgConnection) {
    let cooks = Cook::all(db_connection);
    let role_as_cook = Role::get_by_label(db_connection, String::from("cook")).unwrap();
    let role_as_administrator =
        Role::get_by_label(db_connection, String::from("administrator")).unwrap();

    for cook in cooks {
        if &cook.name == "John Doe" || &cook.name == "Jack Slave" {
            diesel::insert_into(all_cook_role)
                .values((
                    cook_roles::role_id.eq(&role_as_administrator.id),
                    cook_roles::cook_id.eq(cook.id),
                ))
                .returning(cook_roles::columns::id)
                .get_result::<i32>(db_connection)
                .expect("Error creating relation between cook and role");
        } else {
            diesel::insert_into(all_cook_role)
                .values((
                    cook_roles::role_id.eq(&role_as_cook.id),
                    cook_roles::cook_id.eq(cook.id),
                ))
                .returning(cook_roles::columns::id)
                .get_result::<i32>(db_connection)
                .expect("Error creating relation between cook and role");
        }
    }

    println!("Table cook_role has been populated.");
}

pub fn populate_time_to_cook_labels(db_connection: &PgConnection) {
    let names = ["fast", "medium", "long"];

    for name in names.iter() {
        diesel::insert_into(all_time_to_cook_label)
            .values(time_to_cook_labels::name.eq(name))
            .execute(db_connection)
            .expect("Error inserting new time_to_cook_label");
    }

    println!("Table time_to_cook_label has been populated.");
}

pub fn populate_recipes(db_connection: &PgConnection) {
    let cooks = all_cook
        .limit(5)
        .load::<Cook>(db_connection)
        .expect("Error getting cooks");
    let cook0 = cooks.get(0).expect("Can't get the zeroth cook");
    let cook1 = cooks.get(1).expect("Can't get the first cook");
    let cook2 = cooks.get(2).expect("Can't get the second cook");
    let cook3 = cooks.get(3).expect("Can't get the third cook");

    let time_to_cook_label_fast =
        TimeToCookLabel::get_by_name(&db_connection, String::from("fast"))
            .expect("`fast` time to cook label");
    let time_to_cook_label_medium =
        TimeToCookLabel::get_by_name(&db_connection, String::from("medium"))
            .expect("`medium` time to cook label");
    let time_to_cook_label_long =
        TimeToCookLabel::get_by_name(&db_connection, String::from("long"))
            .expect("`long` time to cook label");

    let new_recipe0 = NewRecipe {
        name: String::from("lasagnes"),
        time_to_cook_label_id: Some(time_to_cook_label_long.id),
        content: String::from("
            # Préparation de la bolognaise.
            Coupez l'oignon en petits morceaux et faites les revenir dans de l'huile d'olive.
            Quand les oignons ont bien bruni, ajoutez la viande haché.
            Faites cuire à feu moyen puis ajouter la sauce tomate.
            Préchauffez le four à 180°C.

            # Préparation de la béchamel
            Faites fondre le beurre à feu vif.
            Une fois fondu, rajoutez la farine puis remuez avec un fouet à feu moyen.
            Quand le mélange est homogène rajoutez progressivement le lait sans arrêter de fouetter pour éviter les grumeaux.
            Continuez de remuer jusqu'à ce que la béchamel s'épaississe.

            # Préparation des feuilles de lasagne
            Portez ébullition l'eau afin de pouvoir.
            Baignez les feuilles de lasagne jusqu'à qu'elles soient ramollies
            Sortez les de l'eau.

            # Assemblage
            Placez des feuilles de lasagne jusqu'à couvrir la surface de votre plât.
            Répartissez une couche bolognaise sur les feuilles de lasagnes.
            Faites de même avec la béchamel.
            Placez également des tranches de mozzarella.
            Recommencez ces quatre opérations deux fois.
            Placez ensuite une dernière couche de béchamel.
            Saupoudrez ensuite de parmesan.

            # Cuisson
            Placez au four pendant 45 minutes.
        "),
        cook_id: cook0.id,
    };

    let new_recipe1 = NewRecipe {
        name: String::from("rouleaux de printemps au porc caramélisé"),
        time_to_cook_label_id: Some(time_to_cook_label_medium.id),
        content: String::from("
            # Préparation de la sauce
            Commencez par préparer la sauce, pour cela mélangez le jus de citron vert, la sauce soja et le beurre de cacahouètes ensemble.

            # Préparation du porc caramélisé
            Découpez le porc en petit morceaux de la taille d'une bouchée en prenant soin d'enlever la graisse.
            Versez le sucre en le répartissant bien sur toute la surface d'une poêle et faites le chauffez à feu moyen jusqu'à caramélisation.
            Faites cuire les morceaux de porc dans le caramel.
            Ajoutez l'huile de sésame et faites cuire pendant 20 minutes jusqu'à que le porc ait caramélisé.
            Placez le porc caramélisé dans un petit récipient.

            # Préparation des légumes et du vermicelles de riz
            Découpez les carottes et le demi concombre en fins batônnets
            Faites cuire les vermicelles de riz comme indiqué sur le paquet.
            Préparez des petits morceaux de salade.

            # Assemblage des rouleaux de printemps
            Faites bouillir de l'eau dans une poêle.
            Une fois que l'eau est chaude, trempez une feuille de riz dans la poêle d'eau chaude jusqu'à qu'elle soit souple et légèrement collante.
            Étendez la feuille de riz sur un torchon propre et placez au centre des morceaux de porc au caramel, des vermicelles, des batônnets de carotte et concombre et une cuillère à soupe de sauce.
            Ramenez le bord de la feuille de riz le plus éloigné de vous sur le tas que vous venez de former.
            Ramenez ensuite les cotés droits et gauche sur le centre et ensuite roulez le tout.
            Répétez ces opérations jusqu'à avoir consommé toutes vos feuille de riz.
        "),
        cook_id: cook1.id,
    };

    let new_recipe2 = NewRecipe {
        name: String::from("pancakes"),
        time_to_cook_label_id: Some(time_to_cook_label_medium.id),
        content: String::from("
            # Préparation de la pâte.
            Faites fondre le beurre.
            Pendant ce temps, mélangez dans un saladier la farine, le sucre et la levure et formez un puit.
            Ajoutez les œufs dans le puit et fouttez.
            Versez ensuite le beurre fondu.
            Délayez ensuite progressivement le mélange avec le lait afin d'éviter les grumeaux.
            Laissez reposer la pâte au minimum une heure.

            # Cuisson des pancakes
            Chauffez à feu moyen une poêle légèrement huilée.
            Déposez une demi louche de pâte à pancake dans la poêle et laissez cuire jusqu'à que des trous se forment sur la surface du pancake.
            Retournez le pancake et laissez le cuire environ 45 secondes.
            Retirez le pancake.
            Répétez ces opérations jusqu'à ne plus avoir de pâte.

        "),
        cook_id: cook2.id,
    };

    let new_recipe3 = NewRecipe {
        name: String::from("quiche lorraine"),
        time_to_cook_label_id: Some(time_to_cook_label_long.id),
        content: String::from("
            Préchauffez le four à 180°C.
            Battez les œufs comme une omelette avec le sel, le poivre, le lait et la crème fraîche dans un saladier.
            Faites blanchir les oignons.
            Faites revenir les lardons avec les oignons.
            Étalez la pâte brisée et piquez sur toute la surface avec une fourchette.
            Versez le contenu du saladier sur la pâte brisée et répartissez les oignons et lardons dessus.
            Placez au four pendant 45 à 50 minutes.
        "),
        cook_id: cook2.id,
    };

    let new_recipe4 = NewRecipe {
        name: String::from("jacked potatoes"),
        time_to_cook_label_id: Some(time_to_cook_label_long.id),
        content: String::from("
            # Préparation des pommes de terre
            Préchauffez le four à 180°C.
            Lavez et brossez les pommes de terre mais ne les pelez pas.
            Percez la peau du dessus des pommes de terre.
            Enfournez les pommes de terre pendant 1 heure.

            # Préparation de la farce
            Pelez et émincez les oignons.
            Faites revenir oignons dans une sauteuse.
            Découpez les blancs de poulets en lanières, et ajoutez les dans la sauteuse avec la pâte de curry.
            Mélangez, couvrez et prolongez la cuisson de cinq minutes.
            Quand les pommes de terre sont cuites, retirez les du four.
            À l'aide d'un couteau, découpez un chapeau et retirez la chair avec une petite cuillère en évitant d'abimer la peau.
            Versez cette chair dans un saladier et mélangez-la au fromage, au poulet et à la coriandre.
            Garnissez les coques de pommes de terre de ce mélange.
            Repassez les pommes de terre au four pendant 10 minutes.
        "),
        cook_id: cook3.id,
    };

    let new_recipe5 = NewRecipe {
        name: String::from("tartines chèvre et poireaux"),
        time_to_cook_label_id: Some(time_to_cook_label_fast.id),
        content: String::from(
            "
            Lavez et émincez finement les blancs de poireaux.
            Faites revenir les poireaux et les lardons dans une sauteuse.
            Assaisonnez.
            Toastez les tranches de pains.
            Tartinez les de fromage de chèvre.
            Garnissez les avec les poireaux et lardons.
        ",
        ),
        cook_id: cook3.id,
    };

    let new_recipes_with_dates = vec![
        (
            new_recipe0,
            NaiveDate::from_ymd(2019, 4, 3).and_hms(9, 10, 11),
            None,
        ),
        (
            new_recipe1,
            NaiveDate::from_ymd(2019, 6, 23).and_hms(10, 10, 10),
            Some(NaiveDate::from_ymd(2019, 6, 27).and_hms(13, 40, 1)),
        ),
        (
            new_recipe2,
            NaiveDate::from_ymd(2019, 2, 24).and_hms(9, 45, 53),
            Some(NaiveDate::from_ymd(2019, 8, 31).and_hms(12, 50, 4)),
        ),
        (
            new_recipe3,
            NaiveDate::from_ymd(2019, 1, 18).and_hms(16, 15, 20),
            Some(NaiveDate::from_ymd(2019, 5, 27).and_hms(14, 24, 3)),
        ),
        (
            new_recipe4,
            NaiveDate::from_ymd(2019, 2, 26).and_hms(16, 15, 20),
            None,
        ),
        (
            new_recipe5,
            NaiveDate::from_ymd(2019, 5, 26).and_hms(16, 15, 20),
            None,
        ),
    ];

    for new_recipe_with_dates in new_recipes_with_dates {
        if let Ok(created_recipe) = NewRecipe::create(db_connection, new_recipe_with_dates.0) {
            println!("The new recipe {} has been created", created_recipe.name);
            Recipe::update(
                db_connection,
                Recipe {
                    id: created_recipe.id,
                    name: created_recipe.name,
                    time_to_cook_label_id: created_recipe.time_to_cook_label_id,
                    content: created_recipe.content,
                    creation_date: new_recipe_with_dates.1,
                    update_date: new_recipe_with_dates.2,
                    cook_id: created_recipe.cook_id,
                },
            )
            .expect("Failed to update recipe");
        }
    }

    println!("Table recipe has been populated.");
}

pub fn populate_recipe_cooks(db_connection: &PgConnection) {
    let cooks = Cook::all(db_connection);

    let cook0 = cooks.get(0).expect("Can't get the zeroth cook");
    let cook1 = cooks.get(1).expect("Can't get the first cook");
    let cook2 = cooks.get(2).expect("Can't get the second cook");
    let cook3 = cooks.get(3).expect("Can't get the third cook");
    let cook4 = cooks.get(4).expect("Can't get the fourth cook");
    let cook5 = cooks.get(5).expect("Can't get the fifth cook");
    let cook6 = cooks.get(6).expect("Can't get the sixth cook");

    let recipes = Recipe::all(db_connection);

    let recipe0 = recipes.get(0).expect("Can't get the zeroth recipe");
    let recipe1 = recipes.get(1).expect("Can't get the first recipe");
    let recipe2 = recipes.get(2).expect("Can't get the second recipe");
    let recipe3 = recipes.get(3).expect("Can't get the third recipe");
    let recipe4 = recipes.get(4).expect("Can't get the fourth recipe");
    let recipe5 = recipes.get(5).expect("Can't get the fifth recipe");

    diesel::insert_into(all_recipe_cook)
        .values((
            recipe_cooks::use_date.eq(NaiveDate::from_ymd(2019, 7, 5)),
            recipe_cooks::recipe_id.eq(&recipe0.id),
            recipe_cooks::cook_id.eq(&cook0.id),
        ))
        .returning(recipe_cooks::columns::id)
        .get_result::<i32>(db_connection)
        .expect("Error creating relation between recipe and cook");

    diesel::insert_into(all_recipe_cook)
        .values((
            recipe_cooks::use_date.eq(NaiveDate::from_ymd(2019, 8, 5)),
            recipe_cooks::recipe_id.eq(&recipe2.id),
            recipe_cooks::cook_id.eq(&cook0.id),
        ))
        .returning(recipe_cooks::columns::id)
        .get_result::<i32>(db_connection)
        .expect("Error creating relation between recipe and cook");

    diesel::insert_into(all_recipe_cook)
        .values((
            recipe_cooks::use_date.eq(NaiveDate::from_ymd(2019, 7, 24)),
            recipe_cooks::recipe_id.eq(&recipe4.id),
            recipe_cooks::cook_id.eq(&cook0.id),
        ))
        .returning(recipe_cooks::columns::id)
        .get_result::<i32>(db_connection)
        .expect("Error creating relation between recipe and cook");

    diesel::insert_into(all_recipe_cook)
        .values((
            recipe_cooks::use_date.eq(NaiveDate::from_ymd(2019, 7, 14)),
            recipe_cooks::recipe_id.eq(&recipe1.id),
            recipe_cooks::cook_id.eq(&cook1.id),
        ))
        .returning(recipe_cooks::columns::id)
        .get_result::<i32>(db_connection)
        .expect("Error creating relation between recipe and cook");

    diesel::insert_into(all_recipe_cook)
        .values((
            recipe_cooks::use_date.eq(NaiveDate::from_ymd(2019, 7, 15)),
            recipe_cooks::recipe_id.eq(&recipe3.id),
            recipe_cooks::cook_id.eq(&cook1.id),
        ))
        .returning(recipe_cooks::columns::id)
        .get_result::<i32>(db_connection)
        .expect("Error creating relation between recipe and cook");

    diesel::insert_into(all_recipe_cook)
        .values((
            recipe_cooks::use_date.eq(NaiveDate::from_ymd(2019, 8, 14)),
            recipe_cooks::recipe_id.eq(&recipe5.id),
            recipe_cooks::cook_id.eq(&cook1.id),
        ))
        .returning(recipe_cooks::columns::id)
        .get_result::<i32>(db_connection)
        .expect("Error creating relation between recipe and cook");

    diesel::insert_into(all_recipe_cook)
        .values((
            recipe_cooks::use_date.eq(NaiveDate::from_ymd(2019, 7, 12)),
            recipe_cooks::recipe_id.eq(&recipe2.id),
            recipe_cooks::cook_id.eq(&cook2.id),
        ))
        .returning(recipe_cooks::columns::id)
        .get_result::<i32>(db_connection)
        .expect("Error creating relation between recipe and cook");

    diesel::insert_into(all_recipe_cook)
        .values((
            recipe_cooks::use_date.eq(NaiveDate::from_ymd(2019, 9, 11)),
            recipe_cooks::recipe_id.eq(&recipe3.id),
            recipe_cooks::cook_id.eq(&cook3.id),
        ))
        .returning(recipe_cooks::columns::id)
        .get_result::<i32>(db_connection)
        .expect("Error creating relation between recipe and cook");

    diesel::insert_into(all_recipe_cook)
        .values((
            recipe_cooks::use_date.eq(NaiveDate::from_ymd(2019, 11, 11)),
            recipe_cooks::recipe_id.eq(&recipe5.id),
            recipe_cooks::cook_id.eq(&cook3.id),
        ))
        .returning(recipe_cooks::columns::id)
        .get_result::<i32>(db_connection)
        .expect("Error creating relation between recipe and cook");

    diesel::insert_into(all_recipe_cook)
        .values((
            recipe_cooks::use_date.eq(NaiveDate::from_ymd(2019, 10, 5)),
            recipe_cooks::recipe_id.eq(&recipe4.id),
            recipe_cooks::cook_id.eq(&cook4.id),
        ))
        .returning(recipe_cooks::columns::id)
        .get_result::<i32>(db_connection)
        .expect("Error creating relation between recipe and cook");

    diesel::insert_into(all_recipe_cook)
        .values((
            recipe_cooks::use_date.eq(NaiveDate::from_ymd(2019, 10, 6)),
            recipe_cooks::recipe_id.eq(&recipe4.id),
            recipe_cooks::cook_id.eq(&cook4.id),
        ))
        .returning(recipe_cooks::columns::id)
        .get_result::<i32>(db_connection)
        .expect("Error creating relation between recipe and cook");

    diesel::insert_into(all_recipe_cook)
        .values((
            recipe_cooks::use_date.eq(NaiveDate::from_ymd(2019, 10, 7)),
            recipe_cooks::recipe_id.eq(&recipe4.id),
            recipe_cooks::cook_id.eq(&cook4.id),
        ))
        .returning(recipe_cooks::columns::id)
        .get_result::<i32>(db_connection)
        .expect("Error creating relation between recipe and cook");

    diesel::insert_into(all_recipe_cook)
        .values((
            recipe_cooks::use_date.eq(NaiveDate::from_ymd(2019, 12, 6)),
            recipe_cooks::recipe_id.eq(&recipe5.id),
            recipe_cooks::cook_id.eq(&cook5.id),
        ))
        .returning(recipe_cooks::columns::id)
        .get_result::<i32>(db_connection)
        .expect("Error creating relation between recipe and cook");

    diesel::insert_into(all_recipe_cook)
        .values((
            recipe_cooks::use_date.eq(NaiveDate::from_ymd(2019, 11, 5)),
            recipe_cooks::recipe_id.eq(&recipe0.id),
            recipe_cooks::cook_id.eq(&cook5.id),
        ))
        .returning(recipe_cooks::columns::id)
        .get_result::<i32>(db_connection)
        .expect("Error creating relation between recipe and cook");

    diesel::insert_into(all_recipe_cook)
        .values((
            recipe_cooks::use_date.eq(NaiveDate::from_ymd(2019, 11, 5)),
            recipe_cooks::recipe_id.eq(&recipe0.id),
            recipe_cooks::cook_id.eq(&cook6.id),
        ))
        .returning(recipe_cooks::columns::id)
        .get_result::<i32>(db_connection)
        .expect("Error creating relation between recipe and cook");

    diesel::insert_into(all_recipe_cook)
        .values((
            recipe_cooks::use_date.eq(NaiveDate::from_ymd(2019, 11, 7)),
            recipe_cooks::recipe_id.eq(&recipe1.id),
            recipe_cooks::cook_id.eq(&cook6.id),
        ))
        .returning(recipe_cooks::columns::id)
        .get_result::<i32>(db_connection)
        .expect("Error creating relation between recipe and cook");

    diesel::insert_into(all_recipe_cook)
        .values((
            recipe_cooks::use_date.eq(NaiveDate::from_ymd(2019, 11, 8)),
            recipe_cooks::recipe_id.eq(&recipe2.id),
            recipe_cooks::cook_id.eq(&cook6.id),
        ))
        .returning(recipe_cooks::columns::id)
        .get_result::<i32>(db_connection)
        .expect("Error creating relation between recipe and cook");

    diesel::insert_into(all_recipe_cook)
        .values((
            recipe_cooks::use_date.eq(NaiveDate::from_ymd(2019, 11, 10)),
            recipe_cooks::recipe_id.eq(&recipe3.id),
            recipe_cooks::cook_id.eq(&cook6.id),
        ))
        .returning(recipe_cooks::columns::id)
        .get_result::<i32>(db_connection)
        .expect("Error creating relation between recipe and cook");

    diesel::insert_into(all_recipe_cook)
        .values((
            recipe_cooks::use_date.eq(NaiveDate::from_ymd(2019, 11, 11)),
            recipe_cooks::recipe_id.eq(&recipe4.id),
            recipe_cooks::cook_id.eq(&cook6.id),
        ))
        .returning(recipe_cooks::columns::id)
        .get_result::<i32>(db_connection)
        .expect("Error creating relation between recipe and cook");

    diesel::insert_into(all_recipe_cook)
        .values((
            recipe_cooks::use_date.eq(NaiveDate::from_ymd(2019, 11, 17)),
            recipe_cooks::recipe_id.eq(&recipe5.id),
            recipe_cooks::cook_id.eq(&cook6.id),
        ))
        .returning(recipe_cooks::columns::id)
        .get_result::<i32>(db_connection)
        .expect("Error creating relation between recipe and cook");

    println!("Table recipe_cooks has been populated.");
}

pub fn populate_ingredients(db_connection: &PgConnection) {
    let ingredient_names = [
        "sauce tomate",
        "pâte à lasagne",
        "viande haché",
        "oignon",
        "lait",
        "farine",
        "beurre",
        "parmesan",
        "mozzarella",
        "sauce soja",
        "jus de citron vert",
        "beurre de cacahouètes",
        "échine de porc",
        "sucre",
        "huile de sésame",
        "carotte",
        "concombre",
        "vermicelle de riz",
        "salade",
        "feuille de riz",
        "levure",
        "œuf",
        "huile d'olive",
        "sel",
        "poivre",
        "crême fraîche",
        "lardon",
        "pâte brisée",
        "pomme de terre",
        "blanc de poulet",
        "pâte de curry",
        "fromage frais",
        "coriandre",
        "fromage de chèvre",
        "blanc de poireaux",
        "pain tranché",
    ];

    for ingredient_name in ingredient_names.iter() {
        NewIngredient::create(
            db_connection,
            NewIngredient {
                name: (*ingredient_name).to_string(),
            },
        )
        .expect("Failed to create ingredient");
    }
}

pub fn populate_units(db_connection: &PgConnection) {
    let unit0 = NewUnit {
        name: String::from("gramme"),
        symbol: Some(String::from("g")),
    };
    let unit1 = NewUnit {
        name: String::from("centilitre"),
        symbol: Some(String::from("cl")),
    };
    let unit2 = NewUnit {
        name: String::from("boule"),
        symbol: None,
    };
    let unit3 = NewUnit {
        name: String::from("cuillère à soupe"),
        symbol: Some(String::from("CS")),
    };
    let unit4 = NewUnit {
        name: String::from("cuillère à café"),
        symbol: Some(String::from("CC")),
    };
    let unit5 = NewUnit {
        name: String::from("tranche"),
        symbol: None,
    };
    let unit6 = NewUnit {
        name: String::from("tasse à café"),
        symbol: None,
    };
    let unit7 = NewUnit {
        name: String::from("paquet"),
        symbol: None,
    };

    let units = vec![unit0, unit1, unit2, unit3, unit4, unit5, unit6, unit7];

    for unit in units {
        NewUnit::create(db_connection, unit).expect("Failed to create unit");
    }
}

pub fn populate_ingredient_recipe_units(db_connection: &PgConnection) {
    let recipe_lasagnes = Recipe::get_by_name(db_connection, String::from("lasagnes")).unwrap();
    let recipe_rouleaux_de_printemps = Recipe::get_by_name(
        db_connection,
        String::from("rouleaux de printemps au porc caramélisé"),
    )
    .unwrap();
    let recipe_pancakes = Recipe::get_by_name(db_connection, String::from("pancakes")).unwrap();
    let recipe_quiche_lorraine =
        Recipe::get_by_name(db_connection, String::from("quiche lorraine")).unwrap();
    let recipe_jacked_potatoes =
        Recipe::get_by_name(db_connection, String::from("jacked potatoes")).unwrap();
    let recipe_tartines =
        Recipe::get_by_name(db_connection, String::from("tartines chèvre et poireaux")).unwrap();

    let unit_gramme =
        Unit::get_by_name(db_connection, String::from("gramme")).expect("Failed to retrieve unit");
    let unit_centilitre = Unit::get_by_name(db_connection, String::from("centilitre"))
        .expect("Failed to retrieve unit");
    let unit_boule =
        Unit::get_by_name(db_connection, String::from("boule")).expect("Failed to retrieve unit");
    let unit_cuillere_a_soupe = Unit::get_by_name(db_connection, String::from("cuillère à soupe"))
        .expect("Failed to retrieve unit");
    let unit_cuillere_a_cafe = Unit::get_by_name(db_connection, String::from("cuillère à café"))
        .expect("Failed to retrieve unit");
    let unit_tranche =
        Unit::get_by_name(db_connection, String::from("tranche")).expect("Failed to retrieve unit");
    let unit_tasse_a_cafe = Unit::get_by_name(db_connection, String::from("tasse à café"))
        .expect("Failed to retrieve unit");
    let unit_paquet =
        Unit::get_by_name(db_connection, String::from("paquet")).expect("Failed to retrieve unit");

    let ingredient_sauce_tomate =
        Ingredient::get_by_name(db_connection, String::from("sauce tomate"))
            .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(6),
            ingredient_recipe_units::quantity.eq(Some(String::from("600"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_sauce_tomate.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_lasagnes.id),
            ingredient_recipe_units::unit_id.eq(Some(&unit_gramme.id)),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_pate_lasagne =
        Ingredient::get_by_name(db_connection, String::from("pâte à lasagne"))
            .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(6),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_pate_lasagne.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_lasagnes.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_viande_hachee =
        Ingredient::get_by_name(db_connection, String::from("viande haché"))
            .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(6),
            ingredient_recipe_units::quantity.eq(Some(String::from("600"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_viande_hachee.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_lasagnes.id),
            ingredient_recipe_units::unit_id.eq(Some(&unit_gramme.id)),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_oignon = Ingredient::get_by_name(db_connection, String::from("oignon"))
        .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(6),
            ingredient_recipe_units::quantity.eq(Some(String::from("1"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_oignon.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_lasagnes.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_lait = Ingredient::get_by_name(db_connection, String::from("lait"))
        .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(6),
            ingredient_recipe_units::quantity.eq(Some(String::from("50"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_lait.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_lasagnes.id),
            ingredient_recipe_units::unit_id.eq(Some(&unit_centilitre.id)),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_farine = Ingredient::get_by_name(db_connection, String::from("farine"))
        .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(6),
            ingredient_recipe_units::quantity.eq(Some(String::from("3"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_farine.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_lasagnes.id),
            ingredient_recipe_units::unit_id.eq(Some(&unit_cuillere_a_soupe.id)),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_beurre = Ingredient::get_by_name(db_connection, String::from("beurre"))
        .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(6),
            ingredient_recipe_units::quantity.eq(Some(String::from("125"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_beurre.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_lasagnes.id),
            ingredient_recipe_units::unit_id.eq(Some(&unit_gramme.id)),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_parmesan = Ingredient::get_by_name(db_connection, String::from("parmesan"))
        .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(6),
            ingredient_recipe_units::quantity.eq(Some(String::from("75"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_parmesan.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_lasagnes.id),
            ingredient_recipe_units::unit_id.eq(Some(&unit_gramme.id)),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_mozzarella = Ingredient::get_by_name(db_connection, String::from("mozzarella"))
        .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(6),
            ingredient_recipe_units::quantity.eq(Some(String::from("1"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_mozzarella.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_lasagnes.id),
            ingredient_recipe_units::unit_id.eq(Some(&unit_boule.id)),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_sauce_soja = Ingredient::get_by_name(db_connection, String::from("sauce soja"))
        .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_sauce_soja.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_rouleaux_de_printemps.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_jus_de_citron_vert =
        Ingredient::get_by_name(db_connection, String::from("jus de citron vert"))
            .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_jus_de_citron_vert.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_rouleaux_de_printemps.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_beurre_de_cacahouetes =
        Ingredient::get_by_name(db_connection, String::from("beurre de cacahouètes"))
            .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("1/4"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_beurre_de_cacahouetes.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_rouleaux_de_printemps.id),
            ingredient_recipe_units::unit_id.eq(Some(&unit_tasse_a_cafe.id)),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_echine_de_porc =
        Ingredient::get_by_name(db_connection, String::from("échine de porc"))
            .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("500"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_echine_de_porc.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_rouleaux_de_printemps.id),
            ingredient_recipe_units::unit_id.eq(Some(&unit_gramme.id)),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_sucre = Ingredient::get_by_name(db_connection, String::from("sucre"))
        .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("3/4"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_sucre.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_rouleaux_de_printemps.id),
            ingredient_recipe_units::unit_id.eq(Some(&unit_tasse_a_cafe.id)),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_huile_de_sesame =
        Ingredient::get_by_name(db_connection, String::from("huile de sésame"))
            .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("1"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_huile_de_sesame.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_rouleaux_de_printemps.id),
            ingredient_recipe_units::unit_id.eq(Some(&unit_cuillere_a_cafe.id)),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_carotte = Ingredient::get_by_name(db_connection, String::from("carotte"))
        .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("2"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_carotte.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_rouleaux_de_printemps.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_concombre = Ingredient::get_by_name(db_connection, String::from("concombre"))
        .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("1/2"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_concombre.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_rouleaux_de_printemps.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_vermicelle_de_riz =
        Ingredient::get_by_name(db_connection, String::from("vermicelle de riz"))
            .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_vermicelle_de_riz.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_rouleaux_de_printemps.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_salade = Ingredient::get_by_name(db_connection, String::from("salade"))
        .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_salade.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_rouleaux_de_printemps.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_feuille_de_riz =
        Ingredient::get_by_name(db_connection, String::from("feuille de riz"))
            .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("12"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_feuille_de_riz.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_rouleaux_de_printemps.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("60"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_beurre.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_pancakes.id),
            ingredient_recipe_units::unit_id.eq(&unit_gramme.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("250"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_farine.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_pancakes.id),
            ingredient_recipe_units::unit_id.eq(&unit_gramme.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("30"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_sucre.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_pancakes.id),
            ingredient_recipe_units::unit_id.eq(&unit_gramme.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_levure = Ingredient::get_by_name(db_connection, String::from("levure"))
        .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("1"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_levure.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_pancakes.id),
            ingredient_recipe_units::unit_id.eq(&unit_paquet.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_oeuf = Ingredient::get_by_name(db_connection, String::from("œuf"))
        .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("2"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_oeuf.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_pancakes.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("30"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_lait.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_pancakes.id),
            ingredient_recipe_units::unit_id.eq(&unit_centilitre.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_huile_d_olive =
        Ingredient::get_by_name(db_connection, String::from("huile d'olive"))
            .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("1"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_huile_d_olive.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_pancakes.id),
            ingredient_recipe_units::unit_id.eq(&unit_cuillere_a_soupe.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(6),
            ingredient_recipe_units::quantity.eq(Some(String::from("3"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_oeuf.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_quiche_lorraine.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_sel = Ingredient::get_by_name(db_connection, String::from("sel"))
        .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(6),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_sel.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_quiche_lorraine.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_poivre = Ingredient::get_by_name(db_connection, String::from("poivre"))
        .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(6),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_poivre.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_quiche_lorraine.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(6),
            ingredient_recipe_units::quantity.eq(Some(String::from("20"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_lait.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_quiche_lorraine.id),
            ingredient_recipe_units::unit_id.eq(&unit_centilitre.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_creme_fraiche =
        Ingredient::get_by_name(db_connection, String::from("crême fraîche"))
            .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(6),
            ingredient_recipe_units::quantity.eq(Some(String::from("20"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_creme_fraiche.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_quiche_lorraine.id),
            ingredient_recipe_units::unit_id.eq(&unit_centilitre.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(6),
            ingredient_recipe_units::quantity.eq(Some(String::from("1"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_oignon.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_quiche_lorraine.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_lardon = Ingredient::get_by_name(db_connection, String::from("lardon"))
        .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(6),
            ingredient_recipe_units::quantity.eq(Some(String::from("200"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_lardon.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_quiche_lorraine.id),
            ingredient_recipe_units::unit_id.eq(&unit_gramme.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_pate_brisee =
        Ingredient::get_by_name(db_connection, String::from("pâte brisée"))
            .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(6),
            ingredient_recipe_units::quantity.eq(Some(String::from("1"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_pate_brisee.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_quiche_lorraine.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_pomme_de_terre =
        Ingredient::get_by_name(db_connection, String::from("pomme de terre"))
            .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("4"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_pomme_de_terre.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_jacked_potatoes.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("2"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_oignon.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_jacked_potatoes.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_blanc_de_poulet =
        Ingredient::get_by_name(db_connection, String::from("blanc de poulet"))
            .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("3"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_blanc_de_poulet.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_jacked_potatoes.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_pate_de_curry =
        Ingredient::get_by_name(db_connection, String::from("pâte de curry"))
            .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("1"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_pate_de_curry.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_jacked_potatoes.id),
            ingredient_recipe_units::unit_id.eq(&unit_cuillere_a_soupe.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_fromage_frais =
        Ingredient::get_by_name(db_connection, String::from("fromage frais"))
            .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("150"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_fromage_frais.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_jacked_potatoes.id),
            ingredient_recipe_units::unit_id.eq(&unit_gramme.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_coriandre = Ingredient::get_by_name(db_connection, String::from("coriandre"))
        .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("2"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_coriandre.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_jacked_potatoes.id),
            ingredient_recipe_units::unit_id.eq(&unit_cuillere_a_soupe.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_fromage_de_chevre =
        Ingredient::get_by_name(db_connection, String::from("fromage de chèvre"))
            .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("150"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_fromage_de_chevre.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_tartines.id),
            ingredient_recipe_units::unit_id.eq(&unit_gramme.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_blanc_de_poireaux =
        Ingredient::get_by_name(db_connection, String::from("blanc de poireaux"))
            .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("6"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_blanc_de_poireaux.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_tartines.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("100"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_lardon.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_tartines.id),
            ingredient_recipe_units::unit_id.eq(&unit_gramme.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    let ingredient_pain_tranche =
        Ingredient::get_by_name(db_connection, String::from("pain tranché"))
            .expect("Failed to retrieve ingredient");
    diesel::insert_into(all_ingredient_recipe_unit)
        .values((
            ingredient_recipe_units::portions.eq(4),
            ingredient_recipe_units::quantity.eq(Some(String::from("4"))),
            ingredient_recipe_units::ingredient_id.eq(&ingredient_pain_tranche.id),
            ingredient_recipe_units::recipe_id.eq(&recipe_tartines.id),
            ingredient_recipe_units::unit_id.eq(&unit_tranche.id),
        ))
        .execute(db_connection)
        .expect("Error creating relation between ingredient, recipe and unit");

    println!("Table ingredient_recipe_units has been populated.");
}

pub fn main() {
    let db_connection = establish_db_connection();
    populate_cooks(&db_connection);
    populate_roles(&db_connection);
    populate_cook_roles(&db_connection);
    populate_time_to_cook_labels(&db_connection);
    populate_recipes(&db_connection);
    populate_recipe_cooks(&db_connection);
    populate_ingredients(&db_connection);
    populate_units(&db_connection);
    populate_ingredient_recipe_units(&db_connection);
}
