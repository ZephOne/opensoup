CREATE FUNCTION recipe_creation_date_is_older_than_use_date(recipe_id integer, use_date date) RETURNS boolean AS $$
    BEGIN
        RETURN use_date >= (select date(creation_date) FROM recipes WHERE id = recipe_id);
    END;
$$ LANGUAGE plpgsql;

CREATE TABLE cooks (
    id serial PRIMARY KEY,
    email text NOT NULL UNIQUE,
    name text NOT NULL UNIQUE,
    password text NOT NULL
);

CREATE TABLE roles (
    id serial PRIMARY KEY,
    label text NOT NULL UNIQUE
);

CREATE TABLE cook_roles (
    id serial PRIMARY KEY,
    cook_id integer REFERENCES cooks ON DELETE CASCADE NOT NULL,
    role_id  integer REFERENCES roles ON DELETE CASCADE NOT NULL
);

CREATE TABLE time_to_cook_labels (
    id serial PRIMARY KEY,
    name text NOT NULL UNIQUE
);

CREATE TABLE recipes (
    id serial PRIMARY KEY,
    name text NOT NULL UNIQUE,
    time_to_cook_label_id integer,
    content text NOT NULL,
    creation_date timestamp NOT NULL DEFAULT now(),
    update_date timestamp,
    cook_id integer REFERENCES cooks ON DELETE CASCADE NOT NULL
);

CREATE TABLE recipe_cooks (
    id serial PRIMARY KEY,
    use_date date NOT NULL,
    recipe_id integer REFERENCES recipes ON DELETE CASCADE NOT NULL,
    cook_id integer REFERENCES cooks on DELETE CASCADE NOT NULL,
    CHECK (recipe_creation_date_is_older_than_use_date(recipe_id, use_date))
);

CREATE TABLE units (
    id serial PRIMARY KEY,
    name text NOT NULL UNIQUE,
    symbol text
);

CREATE TABLE ingredients (
    id serial PRIMARY KEY,
    name text UNIQUE NOT NULL
);

CREATE TABLE ingredient_recipe_units (
    id serial PRIMARY KEY,
    portions integer NOT NULL DEFAULT 4,
    quantity text,
    ingredient_id integer REFERENCES ingredients ON DELETE CASCADE NOT NULL,
    recipe_id integer REFERENCES recipes ON DELETE CASCADE NOT NULL,
    unit_id integer REFERENCES units ON DELETE CASCADE
);
